# Reward Jar

Programming project for group C15 of Advance Programming

## Pipeline & Coverage Status

#### Master Branch
[![pipeline status](https://gitlab.com/c151/reward-jar/badges/master/pipeline.svg)](https://gitlab.com/c151/reward-jar/-/commits/master)
[![coverage report](https://gitlab.com/c151/reward-jar/badges/master/coverage.svg)](https://gitlab.com/c151/reward-jar/-/commits/master)

#### Dev Branch
[![pipeline status](https://gitlab.com/c151/reward-jar/badges/dev/pipeline.svg)](https://gitlab.com/c151/reward-jar/-/commits/dev) 
[![coverage report](https://gitlab.com/c151/reward-jar/badges/dev/coverage.svg)](https://gitlab.com/c151/reward-jar/-/commits/dev)

## API Calling

1. call POST `/api/user/register` and give it a JSON as an input to register a user
3. call GET `/api/user/{email}` to get a user by it's email
4. call DELETE `/api/user/{email}` to delete a user with the given username
5. call POST `/api/merchant/register` and give it a JSON as an input to register a merchant
6. call GET `/api/merchant/{merchant name}` to get a merchant by it's name
7. call GET `/api/merchant/email/{email}` to get a merchant by it's email
8. call DELETE `/api/user/{merchant name}` to delete a merchant with the given name
9. call DELETE `/api/user/email/{email}` to delete a merchant with the given email

## Registration API Request
Route : '/api/user/register' or '/api/merchant/register'
Body :
```json
{
    "email" : "admin@admin.com",
    "password" : "admin",
    "password2" : "admin",
    "firstName" : "admin",
    "lastName" : "admin",
    "phoneNumber" : "0851234567890",
    "merchantName" : "Admin Store"
}
```

## Route Handling

You can create routes as you normally do just make sure you add the path to the .antMatchers() in EnableWebSecurity.java along with their permission (for example permitAll() means that everyone can access the link while hasAnyRole("USER") means only registered user can accessed them)

**NOTE** By default csrf token is already apply, this mean that you will need to add the routes for REST API to be excluded in order for you not to get errors. This also mean that you'll need to use thymeleaf's th: in the html in order to avoid error (not sure about the last part)

## Templates & Static
Place them in resources with their appropriate folders, make sure to have the links be the right one when you call them