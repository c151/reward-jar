package com.c15.rewardjar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RewardJarApplication {

	/**
	 * Application Runner
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(RewardJarApplication.class, args);
	}

}
