package com.c15.rewardjar.controller;

import java.io.IOException;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.service.AppUserService;
import com.c15.rewardjar.service.RegistrationRequestAppUser;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping(path = "/api")
public class ApiController {
    private AppUserService appUserService;

    @PostMapping(path = "/user/register")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public void registerUser(@RequestBody RegistrationRequestAppUser request) {
        appUserService.signUpUser(request);
    }

    @GetMapping(path = "/user/{email}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity getUserByEmail(@PathVariable(value = "email") String email) {
        return ResponseEntity.ok(appUserService.getUserByEmail(email));

    }

    @DeleteMapping(path = "/user/{email}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity deleteUserByUsername(@PathVariable(value = "email") String email) {
        AppUser appUser = appUserService.getUserByEmail(email);
        if (appUser == null) {
            throw new IllegalStateException("There is no such user!");
        }
        appUserService.deleteByEmail(email);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping(path = "/merchant/register")
    @ResponseBody
    @ResponseStatus(HttpStatus.CREATED)
    public void registerMerchant(@RequestBody RegistrationRequestAppUser request) throws IOException {
        appUserService.signUpMerchant(request);
    }

    @GetMapping(path = "/merchant/email/{email}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity getMerchantByEmail(@PathVariable(value = "email") String email) {
        return ResponseEntity.ok(appUserService.getUserByEmail(email));

    }

    @DeleteMapping(path = "/merchant/email/{email}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity deleteMerchantByEmail(@PathVariable(value = "email") String email) throws IllegalStateException {
        AppUser appUser = appUserService.getUserByEmail(email);
        if (appUser == null) {
            throw new IllegalStateException("There is no such user!");
        }
        appUserService.deleteByEmail(email);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/merchant/{merchantName}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity getMerchantByMerchantName(@PathVariable(value = "merchantName") String merchantName) {
        return ResponseEntity.ok(appUserService.getUserByMerchantName(merchantName));

    }

    @DeleteMapping(path = "/merchant/{merchantName}", produces = { "application/json" })
    @ResponseBody
    public ResponseEntity deleteMerchantByMerchantName(@PathVariable(value = "merchantName") String merchantName) {
        AppUser appUser = appUserService.getUserByMerchantName(merchantName);
        if (appUser == null) {
            throw new IllegalStateException("There is no such user!");
        }
        appUserService.deleteByMerchantName(merchantName);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
