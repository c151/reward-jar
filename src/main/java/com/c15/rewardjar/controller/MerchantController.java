package com.c15.rewardjar.controller;

import com.c15.rewardjar.core.fileuploader.FileUpload;
import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantPoints;
import com.c15.rewardjar.model.MerchantVoucher;
import com.c15.rewardjar.repository.AppUserRepository;
import com.c15.rewardjar.repository.MerchantVoucherRepository;
import com.c15.rewardjar.service.MerchantPointService;
import com.c15.rewardjar.service.MerchantVoucherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping(path = "/merchant")
public class MerchantController {
    @Autowired
    private MerchantPointService merchantPointService;

    @Autowired
    private MerchantVoucherService merchantVoucherService;

    @Autowired
    private MerchantVoucherRepository merchantVoucherRepository;

    @Autowired
    private AppUserRepository appUserRepository;

    @ModelAttribute("merchant")
    public AppUser getCurrentMerchant(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser merchant = (AppUser) auth.getPrincipal();
        return merchant;
    }

    @GetMapping("/")
    public String index(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser merchant = (AppUser) auth.getPrincipal();
        model.addAttribute("pointsShared", merchantPointService.getTotalPointsShared(merchant));
        model.addAttribute("voucherShared", merchantVoucherService.getCountMerchantVoucherRedeemed(merchant));
        model.addAttribute("headerUrl", "/image/header-xl.png");
        return "merchant/index";
    }

    @GetMapping("/list-voucher")
    public String viewVoucher(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser merchant = (AppUser) auth.getPrincipal();
        model.addAttribute("voucherList", merchantVoucherService.getListMerchantVoucher(merchant));
        return "merchant/voucher/list";
    }

    @GetMapping("/create-voucher")
    public String createVoucher() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return "merchant/voucher/create";
    }

    @PostMapping(path="/create-voucher-api", produces = { "application/json" })
    public ResponseEntity createVoucherAPI(@RequestParam String voucherName,
                                           @RequestParam String description,
                                           @RequestParam int stock,
                                           @RequestParam int pointRequired,
                                           @RequestParam
                                               @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate expiredDate)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser merchant = (AppUser) auth.getPrincipal();
        MerchantVoucher newVoucher = merchantVoucherService.createVoucher(merchant, voucherName, description, "/image/default-voucher-logo.png",stock, pointRequired,expiredDate);
        return ResponseEntity.ok(newVoucher);
    }

    @PostMapping("/set-voucher-picture/{id}")
    @ResponseBody
    public ResponseEntity setVoucherPicture(@RequestParam("file")MultipartFile file, @PathVariable(value = "id") int id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        MerchantVoucher voucher = merchantVoucherService.getVoucherById(id);
        final File tempFile = FileUpload.convertMultiPartToFile(file);
        new Thread(() -> {
            String voucherPictureURL = FileUpload.uploadFileToService(tempFile);
            voucher.setImgURL(voucherPictureURL);
            merchantVoucherRepository.save(voucher);
        }).start();
        return ResponseEntity.ok(voucher);
    }

    @PostMapping(path="/update-voucher-api", produces = { "application/json" })
    public ResponseEntity updateVoucherAPI(@RequestParam String voucherName,
                                           @RequestParam String description,
                                           @RequestParam String imgURL,
                                           @RequestParam int stock,
                                           @RequestParam int pointRequired,
                                           @RequestParam int id,
                                           @RequestParam
                                               @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate expiredDate)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser merchant = (AppUser) auth.getPrincipal();
        MerchantVoucher voucher = merchantVoucherService.getVoucherById(id);
        if(voucher == null || voucher.getMerchant().getId() != merchant.getId()){
            return ResponseEntity
                    .status(HttpStatus.FORBIDDEN)
                    .body("Update Failed");
        }
        MerchantVoucher updatedVoucher = merchantVoucherService.updateVoucher(id,merchant, voucherName, description, imgURL,stock, pointRequired,expiredDate);
        return ResponseEntity.ok(updatedVoucher);
    }

    @GetMapping("/update-voucher/{id}")
    public String updateVoucher(Model model, @PathVariable(value = "id") int id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        MerchantVoucher voucher = merchantVoucherService.getVoucherById(id);
        AppUser merchant = (AppUser) auth.getPrincipal();
        if(voucher == null || voucher.getMerchant().getId() != merchant.getId()){
            return "redirect:/merchant/list-voucher";
        }
        model.addAttribute("voucher", voucher);
        return "merchant/voucher/update";
    }

    @GetMapping("/give-points")
    public String givePoints() {
        return "merchant/point/give";
    }

    @PostMapping(path = "/scan-points")
    public String scanPoints(@RequestParam(value = "points-given") int pointsGiven, Model model) {
        AppUser merchant = getCurrentMerchant();
        MerchantPoints merchantPoints =
                merchantPointService.createMerchantPoints(merchant, pointsGiven);

        model.addAttribute("pointsGiven", merchantPoints.getPoints());
        model.addAttribute("qrCode", merchantPoints.getQRCode());
        return "merchant/point/scan";
    }

    @GetMapping(path = "/points-api/{qrCode}")
    @ResponseBody
    public Map<String,Object> getMerchantPoints(@PathVariable("qrCode") String qrCode){
        MerchantPoints merchantPoints = merchantPointService.getMerchantPointsByQRCode(qrCode);

        Map<String,Object> response = new HashMap<>();
        response.put("qrCode", merchantPoints.getQRCode());
        response.put("merchantName", merchantPoints.getMerchant().getMerchantName());
        response.put("points", merchantPoints.getPoints());
        response.put("isUsed", merchantPoints.isUsed());
        return response;
    }

    @PostMapping("set-profile-picture")
    @ResponseBody
    public String setProfileImage(@RequestParam("file") MultipartFile file) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser merchant = (AppUser) auth.getPrincipal();
        final File tempFile = FileUpload.convertMultiPartToFile(file);
        new Thread(() -> {
            String profilePictureURL = FileUpload.uploadFileToService(tempFile);
            merchant.setProfilePicture(profilePictureURL);
            appUserRepository.save(merchant);
        }).start();
        return "";
    }

    @GetMapping("profile")
    public String updateProfile(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser merchant = (AppUser) auth.getPrincipal();
        model.addAttribute("merchant", merchant);
        return "merchant/profile";
    }
}
