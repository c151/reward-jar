package com.c15.rewardjar.controller;

import javax.servlet.http.HttpServletRequest;

import com.c15.rewardjar.core.fileuploader.FileUpload;
import com.c15.rewardjar.model.*;
import com.c15.rewardjar.repository.AppUserRepository;
import com.c15.rewardjar.repository.UserVoucherRepository;
import com.c15.rewardjar.service.MerchantVoucherService;
import com.c15.rewardjar.model.UserPoints;
import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.NewUserPoints;
import com.c15.rewardjar.model.UserPoints;
import com.c15.rewardjar.model.UserVoucher;
import com.c15.rewardjar.service.MerchantPointService;
import com.c15.rewardjar.service.UserPointsService;
import com.c15.rewardjar.service.UserVoucherService;
import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path = "/user")
public class UserController {
    @Autowired
    private UserPointsService userPointsService;

    @Autowired
    private UserVoucherService userVoucherService;

    @Autowired
    private MerchantPointService merchantPointService;

    @Autowired
    private MerchantVoucherService merchantVoucherService;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private UserVoucherRepository userVoucherRepository;

    @GetMapping("/")
    public String index(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        List<UserPoints> userPointsList = userPointsService.getListUserPoints(user);
        List<UserVoucher> userVoucherList = (List<UserVoucher>) userVoucherService.getListUserVouchers(user);
        model.addAttribute("user", user);
        model.addAttribute("userPoints", userPointsList);
        model.addAttribute("userVouchers", userVoucherList);
        return "user/index";
    }

    @GetMapping("scan-points")
    public String scanPoints() {
        return "user/scan";
    }

    @PostMapping(path="scan-points-api", produces = { "application/json" })
    public ResponseEntity scanPointAPI(HttpServletRequest request, @RequestParam(value="code") String code) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        NewUserPoints newUserPoints = userPointsService.addPointsByCode(user, code);
        if (newUserPoints == null) return new ResponseEntity(HttpStatus.BAD_REQUEST);
        return ResponseEntity.ok(newUserPoints);
    }

    @GetMapping(path = "my-voucher", produces = {"application/json"})
    public String getUserVouchers(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        HashMap<AppUser, List<UserVoucher>> myVouchers = userVoucherService.getMapUserVouchers(user);
        model.addAttribute("myVouchers", myVouchers);
        return "user/myvoucher";
    }

    @GetMapping(path = "my-voucher/{id}", produces = {"application/json"})
    public String getUserVoucherDetails(@PathVariable(value = "id") String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        UserVoucher myVoucher = userVoucherRepository.findById(Integer.parseInt(id));
        model.addAttribute("myVoucher", myVoucher.getMerchantVoucher());
        return "user/myvoucherdetails";
    }

    @GetMapping(path = "catalog", produces = {"application/json"})
    public String redeemCatalog(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        List<UserPoints> userPoints = userPointsService.getListUserPoints(user);
        HashMap<UserPoints, List<MerchantVoucher>> merchantVouchers = userVoucherService.getListMerchantVouchers(userPoints);
        model.addAttribute("merchantVouchers", merchantVouchers);
        return "user/redeemcatalog";
    }

    @GetMapping(path = "catalog/{merchantName}", produces = {"application/json"})
    public String getVouchersDetails(@PathVariable(value = "merchantName") String merchantName, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        Optional<AppUser> merchant = appUserRepository.findByMerchantName(merchantName);
        if (merchant.isPresent()) {
            AppUser foundMerchant = merchant.get();
            List<MerchantVoucher> vouchers = (List<MerchantVoucher>) merchantVoucherService.getListMerchantVoucher(foundMerchant);
            model.addAttribute("merchant", foundMerchant);
            model.addAttribute("vouchers", vouchers);
            model.addAttribute("points", userPointsService.findByUserAndMerchant(user, foundMerchant));
        }
        return "user/merchantvouchers";
    }

    @GetMapping(path = "catalog/{merchantName}/{id}", produces = {"application/json"})
    public String getVoucherDetails(@PathVariable(value = "merchantName") String merchantName,
                                    @PathVariable(value = "id") String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        Optional<AppUser> merchant = appUserRepository.findByMerchantName(merchantName);
        if (merchant.isPresent()) {
            UserPoints points = userPointsService.findByUserAndMerchant(user, merchant.get());
            MerchantVoucher voucher = merchantVoucherService.getVoucherById(Integer.parseInt(id));
            model.addAttribute("merchant", merchant.get());
            model.addAttribute("points", points);
            model.addAttribute("voucher", voucher);
        }
        return "user/voucherdetails";
    }

    @GetMapping(path = "catalog/{merchantName}/{id}/redeem", produces = {"application/json"})
    public String redeemVoucher(@PathVariable(value = "merchantName") String merchantName,
                                @PathVariable(value = "id") String id, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        MerchantVoucher merchantVoucher = merchantVoucherService.getVoucherById(Integer.parseInt(id));
        UserVoucher redeemedVoucher = userVoucherService.redeemVoucher(user, merchantVoucher);
        UserPoints points = userPointsService.findByUserAndMerchant(user, merchantVoucher.getMerchant());
        model.addAttribute("merchantVoucher", merchantVoucher);
        model.addAttribute("points", points);
        if (redeemedVoucher == null) {
            return "user/vouchernotredeemed";
        }
        model.addAttribute("redeemedVoucher", redeemedVoucher);
        return "user/voucherredeemed";
    }

    @PostMapping("set-profile-picture")
    @ResponseBody
    public String setProfileImage(@RequestParam("file")MultipartFile file) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        final File tempFile = FileUpload.convertMultiPartToFile(file);
        new Thread(() -> {
            String profilePictureURL = FileUpload.uploadFileToService(tempFile);
            user.setProfilePicture(profilePictureURL);
            appUserRepository.save(user);
        }).start();
        return "";
    }

    @GetMapping("profile")
    public String updateProfile(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        AppUser user = (AppUser) auth.getPrincipal();
        model.addAttribute("user", user);
        return "user/profile";
    }
}
