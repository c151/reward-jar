package com.c15.rewardjar.controller;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.AppUserRole;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/")
public class WebPageController {

    @GetMapping
    public String index() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth.getPrincipal() instanceof AppUser) {
            AppUser user = (AppUser) auth.getPrincipal();
            if (user.getAppUserRole() == AppUserRole.MERCHANT) {
                return "redirect:/merchant/";
            } else if (user.getAppUserRole() == AppUserRole.USER) {
                return "redirect:/user/";
            }
        }
        return "landingpage/index";
    }

    @GetMapping("login")
    public String loginPage() {
        return "login";
    }

    @GetMapping("user/login")
    public String userLoginPage() { return "landingpage/user-login"; }

    @GetMapping("merchant/login")
    public String merchantLoginPage() { return "landingpage/merchant-login"; }

    @GetMapping("user/register")
    public String registerUserPage() {
        return "landingpage/user-register";
    }

    @PostMapping("user/register")
    public String redirectRegisterUserPage() {
        return "redirect:/user/login";
    }

    @GetMapping("merchant/register")
    public String registerMerchantPage() {
        return "landingpage/merchant-register";
    }

    @PostMapping("merchant/register")
    public String redirectRegisterMerchantPage() {
        return "redirect:/merchant/login";
    }

}
