package com.c15.rewardjar.core.fileuploader;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;

public class FileUpload {
    public static String uploadFileToService(File file) {
        String serviceURI = "http://34.101.125.106";
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost uploadFile = new HttpPost(serviceURI);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        try {
            builder.addBinaryBody("file", new FileInputStream(file),
                    ContentType.APPLICATION_OCTET_STREAM, file.getName());
            HttpEntity multipart = builder.build();
            uploadFile.setEntity(multipart);
            CloseableHttpResponse response = httpClient.execute(uploadFile);
            String responseBody = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);
            return String.format("%s/uploads/%s", serviceURI, responseBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static File convertMultiPartToFile (MultipartFile sourceFile) {
        String fileName = sourceFile.getOriginalFilename();
        String prefix = fileName.substring(fileName.lastIndexOf("."));
        File file = null;
        try {
            file = File.createTempFile(fileName, prefix);
            sourceFile.transferTo(file);
        } catch (Exception e){
            e.printStackTrace();
        }
        return file;
    }
}
