package com.c15.rewardjar.core.generator;

import java.util.Random;

public class QRCodeGenerator {
    private static final String CHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String generate(int length){
        Random rnd = new Random(System.currentTimeMillis());
        String result = "";
        for(int i = 0; i < length; i++){
            result += CHARSET.charAt(rnd.nextInt(36));
        }
        return result;
    }
}
