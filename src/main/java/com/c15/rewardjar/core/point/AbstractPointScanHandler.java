package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;

public abstract class AbstractPointScanHandler {
    private AbstractPointScanHandler next;

    public void setNext(AbstractPointScanHandler next) {
        this.next = next;
    }

    public PointAddRequest handle(PointAddRequest request) {
        if (this.next != null) {
            return next.handle(request);
        }
        return request;
    }
}
