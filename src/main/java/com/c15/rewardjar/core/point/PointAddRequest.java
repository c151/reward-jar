package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantPoints;
import com.c15.rewardjar.model.UserPoints;
import org.springframework.stereotype.Component;

public class PointAddRequest {
    AppUser user;
    String code;
    UserPoints userPoints;
    int point;
    MerchantPoints merchantPoints;

    public PointAddRequest (AppUser user, String code) {
        this.user = user;
        this.code = code;
    }

    public AppUser getUser() {
        return this.user;
    }

    public String getCode() {
        return this.code;
    }

    public UserPoints getUserPoints() {
        return this.userPoints;
    }

    public int getPoint() {
        return this.point;
    }

    public MerchantPoints getMerchantPoints() { return this.merchantPoints; }

    public void setUserPoints(UserPoints userPoints) {
        this.userPoints = userPoints;
    }

    public void setPoint(int point) { this.point = point; }

    public void setMerchantPoints(MerchantPoints merchantPoints) { this.merchantPoints = merchantPoints; }
}
