package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantPoints;
import com.c15.rewardjar.model.UserPoints;
import com.c15.rewardjar.repository.MerchantPointRepository;
import com.c15.rewardjar.service.MerchantPointService;
import com.c15.rewardjar.service.MerchantPointServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class PointCodeCheckHandler extends AbstractPointScanHandler{
    @Autowired
    private MerchantPointService merchantPointService;

    @Override
    public PointAddRequest handle(PointAddRequest request){
        String code = request.getCode();

        MerchantPoints merchantPoints = merchantPointService.getMerchantPointsByQRCode(code);
        if (merchantPoints != null && !merchantPoints.isUsed()) {
            merchantPointService.updateMerchantPoints(merchantPoints, true);
            request.setMerchantPoints(merchantPoints);
            request.setPoint(merchantPoints.getPoints());
            return super.handle(request);
        }
        return null;
    }
}
