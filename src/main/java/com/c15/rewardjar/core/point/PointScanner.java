package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.repository.UserPointsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PointScanner {
    @Autowired
    AbstractPointScanHandler pointCodeCheckHandler;

    @Autowired
    AbstractPointScanHandler userPointsCheckHandler;

    @Autowired
    AbstractPointScanHandler userPointsAddHandler;

    public PointAddRequest addPoints(AppUser user, String code) {
        pointCodeCheckHandler.setNext(userPointsCheckHandler);
        userPointsCheckHandler.setNext(userPointsAddHandler);
        PointAddRequest request = new PointAddRequest(user, code);
        return pointCodeCheckHandler.handle(request);
    }
}
