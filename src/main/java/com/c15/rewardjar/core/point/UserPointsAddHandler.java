package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.UserPoints;
import com.c15.rewardjar.service.UserPointsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserPointsAddHandler extends  AbstractPointScanHandler{
    @Autowired
    private UserPointsService userPointsService;

    @Override
    public PointAddRequest handle(PointAddRequest request){
        UserPoints userPoints = request.getUserPoints();
        userPoints.addPoint(request.getPoint());
        userPointsService.updateUserPoints(userPoints);

        return super.handle(request);
    }
}
