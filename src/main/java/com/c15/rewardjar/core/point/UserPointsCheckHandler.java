package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantPoints;
import com.c15.rewardjar.model.UserPoints;
import com.c15.rewardjar.repository.UserPointsRepository;
import com.c15.rewardjar.service.UserPointsService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class UserPointsCheckHandler extends  AbstractPointScanHandler{

    @Autowired
    private UserPointsService userPointsService;

    @Override
    public PointAddRequest handle(PointAddRequest request){
        AppUser user = request.getUser();
        MerchantPoints merchantPoints= request.getMerchantPoints();
        AppUser merchant = merchantPoints.getMerchant();

        UserPoints userPoints = userPointsService.findByUserAndMerchant(user, merchant);
        if (userPoints == null) {
            UserPoints newUserPoints = new UserPoints(user, merchant);
            userPointsService.createUserPoints(newUserPoints);
            request.setUserPoints(newUserPoints);
        } else {
            request.setUserPoints(userPoints);
        }

        return super.handle(request);
    }
}
