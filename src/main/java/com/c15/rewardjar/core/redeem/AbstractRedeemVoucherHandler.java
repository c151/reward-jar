package com.c15.rewardjar.core.redeem;

public abstract class AbstractRedeemVoucherHandler {
    private AbstractRedeemVoucherHandler next;

    public void setNext(AbstractRedeemVoucherHandler next) {
        this.next = next;
    }

    public boolean handle(RedeemVoucherRequest request){
        if(this.next != null){
            return next.handle(request);
        }
        return true;

    }

}
