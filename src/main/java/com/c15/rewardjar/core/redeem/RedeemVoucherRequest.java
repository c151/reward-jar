package com.c15.rewardjar.core.redeem;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantVoucher;
import com.c15.rewardjar.model.UserPoints;

public class RedeemVoucherRequest {
    AppUser user;
    MerchantVoucher merchantVoucher;
    UserPoints userPoints;

    public RedeemVoucherRequest (AppUser user, MerchantVoucher merchantVoucher, UserPoints userPoints) {
        this.user = user;
        this.merchantVoucher = merchantVoucher;
        this.userPoints = userPoints;
    }
}
