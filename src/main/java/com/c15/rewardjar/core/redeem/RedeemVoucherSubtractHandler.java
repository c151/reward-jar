package com.c15.rewardjar.core.redeem;

public class RedeemVoucherSubtractHandler extends AbstractRedeemVoucherHandler{

    @Override
    public boolean handle(RedeemVoucherRequest request) {

        // Mengurangi jumlah voucher catalog
        int quantity = request.merchantVoucher.getStock() - 1;
        request.merchantVoucher.setStock(quantity);

        // Mengurangi point user
        int pointRequired = request.merchantVoucher.getPointRequired();
        int point = request.userPoints.getPoint() - pointRequired;
        request.userPoints.setPoint(point);

        return super.handle(request);
    }
}
