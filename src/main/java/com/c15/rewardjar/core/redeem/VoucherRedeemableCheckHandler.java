package com.c15.rewardjar.core.redeem;

public class VoucherRedeemableCheckHandler extends AbstractRedeemVoucherHandler{

    @Override
    public boolean handle(RedeemVoucherRequest request) {
        boolean redeemable = false;

        // Mengecek apakah voucher masih tersedia stocknya
        if (hasStock(request)) {
            // Mengecek apakah voucher user cukup untuk meredeem voucher
            if (hasEnoughPoints(request)) {
                redeemable = true;
            }
        }

        if (redeemable) {
            return super.handle(request);
        }
        return false;
    }

    public boolean hasStock(RedeemVoucherRequest request) {
        return request.merchantVoucher.getStock() > 0;
    }

    public boolean hasEnoughPoints(RedeemVoucherRequest request) {
        int pointRequired = request.merchantVoucher.getPointRequired();
        return request.userPoints.getPoint() >= pointRequired;
    }
}
