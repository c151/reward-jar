package com.c15.rewardjar.core.redeem;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantVoucher;
import com.c15.rewardjar.model.UserPoints;

public class VoucherRedeemer {

    public VoucherRedeemer() {}

    public boolean redeemVoucher(AppUser user, MerchantVoucher merchantVoucher, UserPoints userPoints) {
        AbstractRedeemVoucherHandler voucherRedeemableChecker = new VoucherRedeemableCheckHandler();
        AbstractRedeemVoucherHandler redeemVoucherSubtractor = new RedeemVoucherSubtractHandler();

        voucherRedeemableChecker.setNext(redeemVoucherSubtractor);

        RedeemVoucherRequest request = new RedeemVoucherRequest(user, merchantVoucher, userPoints);
        return voucherRedeemableChecker.handle(request);
    }
}
