package com.c15.rewardjar.core.user;

import com.c15.rewardjar.service.RegistrationRequestAppUser;

public abstract class AbstractUserCheckerHandler {
    private AbstractUserCheckerHandler next;

	public void setNext(AbstractUserCheckerHandler next) {
		this.next = next;
	}

    public boolean handle(RegistrationRequestAppUser request){
        if(this.next != null){
            return next.handle(request);
        }
        return true;

    }

	public AbstractUserCheckerHandler getNext() {
		return next;
	}
}
