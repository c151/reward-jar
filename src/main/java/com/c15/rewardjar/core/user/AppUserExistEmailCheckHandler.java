package com.c15.rewardjar.core.user;

import com.c15.rewardjar.repository.AppUserRepository;
import com.c15.rewardjar.service.RegistrationRequestAppUser;

import org.springframework.stereotype.Component;

@Component
public class AppUserExistEmailCheckHandler extends AbstractUserCheckerHandler {
    private final AppUserRepository appUserRepository;

    @Override
    public boolean handle(RegistrationRequestAppUser request) {
        boolean emailExists = appUserRepository.findByEmail(request.getEmail()).isPresent();
        if (!emailExists) {
            return super.handle(request);

        }
        return false;
    }


    public AppUserRepository getAppUserRepository() {
        return appUserRepository;
    }


	public AppUserExistEmailCheckHandler(AppUserRepository appUserRepository) {
		this.appUserRepository = appUserRepository;
	}

}
