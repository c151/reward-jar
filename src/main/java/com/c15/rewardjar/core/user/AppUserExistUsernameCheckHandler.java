package com.c15.rewardjar.core.user;

import java.util.Objects;

import com.c15.rewardjar.repository.AppUserRepository;
import com.c15.rewardjar.service.RegistrationRequestAppUser;

import org.springframework.stereotype.Component;

@Component
public class AppUserExistUsernameCheckHandler extends AbstractUserCheckerHandler {

    private final AppUserRepository appUserRepository;

    @Override
    public boolean handle(RegistrationRequestAppUser request) {
        String merchantName = request.getMerchantName();
        boolean usernameExists = appUserRepository.findByMerchantName(merchantName).isPresent();
        // System.out.println(Objects.isNull(appUserRepository));
        if (merchantName == null || !usernameExists) {
            return super.handle(request);

        }
        return false;
    }

    public AppUserExistUsernameCheckHandler(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

}
