package com.c15.rewardjar.core.user;

import com.c15.rewardjar.service.EmailValidator;
import com.c15.rewardjar.service.RegistrationRequestAppUser;
import com.c15.rewardjar.service.RegistrationRequestMerchant;

import org.springframework.stereotype.Component;

@Component
public class EmailValidCheckHandler extends AbstractUserCheckerHandler {
    private EmailValidator emailValidator;

    @Override
    public boolean handle(RegistrationRequestAppUser request) {
        boolean isValidEmail = emailValidator.test(request.getEmail());
        // System.out.println(this.getNext());
        if (isValidEmail) {
            return super.handle(request);

        }
        return false;
    }

    public EmailValidCheckHandler(EmailValidator emailValidator) {
        this.emailValidator = emailValidator;
    }
}
