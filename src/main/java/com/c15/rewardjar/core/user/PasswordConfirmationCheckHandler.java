package com.c15.rewardjar.core.user;

import com.c15.rewardjar.service.EmailValidator;
import com.c15.rewardjar.service.RegistrationRequestAppUser;
import com.c15.rewardjar.service.RegistrationRequestMerchant;

import org.springframework.stereotype.Component;

@Component
public class PasswordConfirmationCheckHandler extends AbstractUserCheckerHandler {
    @Override
    public boolean handle(RegistrationRequestAppUser request) {
        boolean isValidEmail = request.getPassword().equals(request.getPassword2());
        if (isValidEmail) {
            return super.handle(request);

        }
        return false;
    }

}
