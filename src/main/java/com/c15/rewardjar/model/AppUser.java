package com.c15.rewardjar.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
public class AppUser extends Auditable implements UserDetails {
    String ROLE_PREFIX = "ROLE_";
    @Id
    @SequenceGenerator(name = "user_sequence", sequenceName = "user_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_sequence")
    private Long id;

    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;

    private String profilePicture;
    private String merchantName;

    private Boolean isVerified;
    private Long point;

    @Enumerated(EnumType.STRING)
    private AppUserRole appUserRole;
    private Boolean locked;
    private Boolean enabled;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        AppUserRole user = appUserRole.USER;
        List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
        if (!user.name().equals(appUserRole.name())) {
            list.add(new SimpleGrantedAuthority(ROLE_PREFIX + appUserRole.name()));
        }
        list.add(new SimpleGrantedAuthority(ROLE_PREFIX + user.name()));
        return list;
    }

    // public void setProfilePicture(String profilePictureUrl) {
    //     this.profilePicture = profilePictureUrl;
    // }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    public String getProfilePicture() { return profilePicture; }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public AppUser(String email, String password, String firstName, String lastName, String phoneNumber,
            AppUserRole appUserRole) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.appUserRole = appUserRole;
        this.isVerified = true;
        this.point = (long) 0;
        this.locked = false;
        this.enabled = true;
    }

    public AppUser(String email, String password, String merchantName, String phoneNumber, AppUserRole appUserRole) {
        this.email = email;
        this.password = password;
        this.merchantName = merchantName;
        this.phoneNumber = phoneNumber;
        this.appUserRole = appUserRole;
        this.isVerified = true;
        this.point = (long) 0;
        this.locked = false;
        this.enabled = true;
    }
}
