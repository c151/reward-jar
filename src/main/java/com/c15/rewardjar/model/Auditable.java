package com.c15.rewardjar.model;


import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;

@Getter
public abstract class Auditable {

    @CreatedDate
    protected Date createdAt;

    @LastModifiedDate
    protected Date lastLogin;

}
