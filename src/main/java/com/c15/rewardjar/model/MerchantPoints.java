package com.c15.rewardjar.model;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "merchant_points")
@Data
@NoArgsConstructor
public class MerchantPoints {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "app_merchant")
    private AppUser merchant;

    @Column(name = "points")
    private int points;

    @Column(name = "qr_code", unique = true, columnDefinition = "TEXT")
    private String QRCode;

    @Column(name = "is_used")
    private boolean isUsed;

    public MerchantPoints(AppUser merchant, int points, String QRCode, boolean isUsed) {
        this.merchant = merchant;
        this.points = points;
        this.QRCode = QRCode;
        this.isUsed = isUsed;
    }
}
