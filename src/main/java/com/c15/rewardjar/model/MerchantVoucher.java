package com.c15.rewardjar.model;

import javax.persistence.*;
import java.time.LocalDate;
import lombok.NoArgsConstructor;
import lombok.Data;

@Entity
@Table(name = "merchant_vouchers")
@Data
@NoArgsConstructor
public class MerchantVoucher {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "app_merchant")
    private AppUser merchant;

    @Column(name = "voucher_name")
    private String voucherName;

    @Column(name = "description")
    private String description;

    @Column(name = "img_url")
    private String imgURL;

    @Column(name = "stock")
    private int stock;

    @Column(name = "point_required")
    private int pointRequired;

    @Column(name = "expired_date")
    private LocalDate expiredDate;

    public MerchantVoucher(AppUser merchant,
                           String voucherName,
                           String description,
                           String imgURL,
                           int stock,
                           int pointRequired,
                           LocalDate expiredDate )
    {
        this.merchant = merchant;
        this.voucherName = voucherName;
        this.description = description;
        this.imgURL = imgURL;
        this.stock = stock;
        this.pointRequired = pointRequired;
        this.expiredDate = expiredDate;
    }
}
