package com.c15.rewardjar.model;

public class NewUserPoints {
    int points;
    AppUser merchant;

    public NewUserPoints(int points,AppUser merchant) {
        this.points = points;
        this.merchant = merchant;
    }

    public int getPoints() {
        return this.points;
    }

    public AppUser getMerchant() {
        return this.merchant;
    }
}
