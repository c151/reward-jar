package com.c15.rewardjar.model;

import javax.persistence.*;

import lombok.NoArgsConstructor;

@Entity
@Table(name = "user_points")
@NoArgsConstructor
public class UserPoints {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "app_user")
    private AppUser user;

    @ManyToOne
    @JoinColumn(name = "merchant")
    private AppUser merchant;

    @Column(name = "point")
    private int point;

    public UserPoints(AppUser user, AppUser merchant) {
        this.user = user;
        this.merchant = merchant;
        this.point = 0;
    }

    public int getId() {
        return this.id;
    }

    public AppUser getUser() {
        return this.user;
    }

    public void setUser(AppUser user){
        this.user = user;
    }

     public AppUser getMerchant() {
         return this.merchant;
     }

     public void setMerchant(AppUser merchant) {
         this.merchant = merchant;
     }

    public int getPoint() {
        return this.point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public void addPoint(int point) {
        this.point = this.point + point;
    }
}
