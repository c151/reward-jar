package com.c15.rewardjar.model;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "user_voucher")
@Data
@NoArgsConstructor
public class UserVoucher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @JoinColumn(name = "merchant_voucher")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private MerchantVoucher merchantVoucher;

    @ManyToOne
    @JoinColumn(name = "app_user")
    private AppUser user;

    public UserVoucher(AppUser user, MerchantVoucher merchantVoucher) {
        this.user = user;
        this.merchantVoucher = merchantVoucher;
    }

}
