package com.c15.rewardjar.repository;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantPoints;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MerchantPointRepository extends JpaRepository<MerchantPoints,String> {
    public List<MerchantPoints> findByMerchantAndIsUsedTrue(AppUser merchant);
    public MerchantPoints findByQRCode(String QRCode);
}
