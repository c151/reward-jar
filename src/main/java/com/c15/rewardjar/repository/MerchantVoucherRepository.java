package com.c15.rewardjar.repository;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantVoucher;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MerchantVoucherRepository extends JpaRepository<MerchantVoucher,String> {
    public Iterable<MerchantVoucher> findByMerchant(AppUser merchant);
    public MerchantVoucher findById(int id);
}
