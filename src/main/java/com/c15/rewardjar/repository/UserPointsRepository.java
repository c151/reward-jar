package com.c15.rewardjar.repository;

import java.util.List;
import java.util.Optional;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.UserPoints;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional(readOnly = true)
public interface UserPointsRepository extends JpaRepository<UserPoints, Integer> {
    public List<UserPoints> findByUser(AppUser user);
    public UserPoints findByUserAndMerchant(AppUser user, AppUser merchant);
}
