package com.c15.rewardjar.repository;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantVoucher;
import com.c15.rewardjar.model.UserPoints;
import com.c15.rewardjar.model.UserVoucher;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserVoucherRepository extends JpaRepository<UserVoucher, UserPoints> {
    public List<UserVoucher> findByUser(AppUser user);
    public UserVoucher findById(int id);
    public List<UserVoucher> findByMerchantVoucher(MerchantVoucher merchantVoucher);
}
