package com.c15.rewardjar.service;

import com.c15.rewardjar.model.AppUser;

public interface AppUserService {
    /**
     * Signup {@link AppUser} if email doesn't exist yet and valid
     *
     * @param request {@link RegistrationRequestAppUser}
     *
     * @return {@link AppUser}
     */
    // AppUser signUpUser(AppUser appUser);
    AppUser signUpUser(RegistrationRequestAppUser request);

    /**
     * Get {@link AppUser} by it's email
     *
     * @param email String
     *
     * @return {@link AppUser}
     */
    AppUser getUserByEmail(String email);

    /**
     * Delete {@link AppUser} by it's email
     *
     * @param username String
     */
    void deleteByEmail(String email);

    /**
     * Check if the data received is valid or not
     *
     * @param request {@link RegistrationRequestAppUser}
     *
     * @return boolean
     */
    boolean isValidAppUser(RegistrationRequestAppUser request);

    /**
     * Signup {@link AppUser} if email doesn't exist yet and valid and if
     * merchantName doesn't exist yet
     *
     * @param request {@link RegistrationRequestAppUser}
     *
     * @return {@link AppUser}
     */
    // AppUser signUpUser(AppUser appUser);
    AppUser signUpMerchant(RegistrationRequestAppUser request);

    /**
     * Get {@link AppUser} by it's merchantName
     *
     * @param merchantName String
     *
     * @return {@link AppUser}
     */
    AppUser getUserByMerchantName(String merchantName);

    /**
     * Delet{@link AppUser} by it's merchantName
     *
     * @param merchantName String
     */
    void deleteByMerchantName(String merchantName);

}
