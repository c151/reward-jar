package com.c15.rewardjar.service;

import com.c15.rewardjar.core.user.AbstractUserCheckerHandler;
import com.c15.rewardjar.core.user.AppUserExistEmailCheckHandler;
import com.c15.rewardjar.core.user.AppUserExistUsernameCheckHandler;
import com.c15.rewardjar.core.user.EmailValidCheckHandler;
import com.c15.rewardjar.core.user.PasswordConfirmationCheckHandler;
import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.AppUserRole;
import com.c15.rewardjar.repository.AppUserRepository;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class AppUserServiceImpl implements UserDetailsService, AppUserService {

    private final static String USER_NOT_FOUND = "User %s not found";
    private final AppUserRepository appUserRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final EmailValidator emailValidator;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return appUserRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format(USER_NOT_FOUND, username)));
    }

    @Override
    public AppUser signUpUser(RegistrationRequestAppUser request) {
        boolean isValid = isValidAppUser(request);
		System.out.println(isValid);
        if (!isValid) {
            throw new IllegalStateException("User is not valid!");
        }
        String encodedPassword = bCryptPasswordEncoder.encode(request.getPassword());
        AppUser appUser = new AppUser(request.getEmail(), encodedPassword, request.getFirstName(),
                request.getLastName(), request.getPhoneNumber(), AppUserRole.USER);
        appUserRepository.save(appUser);
        return appUser;
    }

    @Override
    public AppUser getUserByEmail(String email) {
        return appUserRepository.findByEmail(email).orElse(null);
    }

    @Override
    public void deleteByEmail(String email) {
        AppUser appUser = this.getUserByEmail(email);
        appUserRepository.deleteById(appUser.getId());

    }

    @Override
    public boolean isValidAppUser(RegistrationRequestAppUser request) {

        AbstractUserCheckerHandler emailValid = new EmailValidCheckHandler(emailValidator);
        AbstractUserCheckerHandler emailExist = new AppUserExistEmailCheckHandler(appUserRepository);
        AbstractUserCheckerHandler merchantNameExist = new AppUserExistUsernameCheckHandler(appUserRepository);
        AbstractUserCheckerHandler passwordSame = new PasswordConfirmationCheckHandler();

        emailValid.setNext(emailExist);
        emailExist.setNext(merchantNameExist);
        merchantNameExist.setNext(passwordSame);

        return emailValid.handle(request);

    }

    @Override
    public AppUser signUpMerchant(RegistrationRequestAppUser request) {
        boolean isValid = isValidAppUser(request);
		System.out.println(isValid);
		System.out.println(!isValid);
        if (!isValid) {
            throw new IllegalStateException("User is not valid!");
        }
        String encodedPassword = bCryptPasswordEncoder.encode(request.getPassword());
        AppUser newMerchant = new AppUser(request.getEmail(), encodedPassword, request.getMerchantName(),
                request.getPhoneNumber(), AppUserRole.MERCHANT);
        appUserRepository.save(newMerchant);
        return newMerchant;
    }

    @Override
    public AppUser getUserByMerchantName(String merchantName) {
        return appUserRepository.findByMerchantName(merchantName).orElse(null);
    }

    @Override
    public void deleteByMerchantName(String merchantName) {
        AppUser appUser = this.getUserByMerchantName(merchantName);
        appUserRepository.deleteById(appUser.getId());

    }

}
