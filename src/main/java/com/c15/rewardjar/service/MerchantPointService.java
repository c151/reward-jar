package com.c15.rewardjar.service;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantPoints;

public interface MerchantPointService {
    MerchantPoints createMerchantPoints(AppUser merchant, int amount);
    int getTotalPointsShared(AppUser merchant);
    MerchantPoints getMerchantPointsByQRCode(String QRCode);
    MerchantPoints updateMerchantPoints(MerchantPoints merchantPoints, boolean isUsed);
}
