package com.c15.rewardjar.service;

import com.c15.rewardjar.core.generator.QRCodeGenerator;
import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantPoints;

import com.c15.rewardjar.repository.MerchantPointRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MerchantPointServiceImpl implements MerchantPointService {
    @Autowired
    private MerchantPointRepository merchantPointRepository;

    @Override
    public MerchantPoints createMerchantPoints(AppUser merchant, int amount){
        String qrCode = QRCodeGenerator.generate(20);
        MerchantPoints points = new MerchantPoints(merchant, amount, qrCode, false);
        merchantPointRepository.save(points);
        return points;
    }

    @Override
    public MerchantPoints getMerchantPointsByQRCode(String QRCode) {
        return merchantPointRepository.findByQRCode(QRCode);
    }

    @Override
    public int getTotalPointsShared(AppUser merchant){
        List<MerchantPoints> listOfPoints =
                merchantPointRepository.findByMerchantAndIsUsedTrue(merchant);
        int totalPointsShared = 0;
        for(MerchantPoints points : listOfPoints){
            totalPointsShared += points.getPoints();
        }
        return totalPointsShared;
    }

    @Override
    public MerchantPoints updateMerchantPoints(MerchantPoints merchantPoints, boolean isUsed) {
        merchantPoints.setUsed(isUsed);
        merchantPointRepository.save(merchantPoints);
        return merchantPoints;
    }
}
