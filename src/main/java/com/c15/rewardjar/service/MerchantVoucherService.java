package com.c15.rewardjar.service;


import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantVoucher;

import java.time.LocalDate;


public interface MerchantVoucherService {

    MerchantVoucher createVoucher(AppUser merchant,
                       String voucherName,
                       String description,
                       String imgURL,
                       int stock,
                       int pointRequired,
                       LocalDate expired);

    MerchantVoucher updateVoucher(int id,
                       AppUser merchant,
                       String voucherName,
                       String description,
                       String imgURL,
                       int stock,
                       int pointRequired,
                       LocalDate expired);

    Iterable<MerchantVoucher> getListMerchantVoucher(AppUser merchant);

    int getCountMerchantVoucherRedeemed(AppUser merchant);

    MerchantVoucher getVoucherById(int id);
}
