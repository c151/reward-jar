package com.c15.rewardjar.service;


import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantVoucher;
import com.c15.rewardjar.repository.MerchantVoucherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class MerchantVoucherServiceImpl implements MerchantVoucherService {

    @Autowired
    private MerchantVoucherRepository merchantVoucherRepository;

    @Autowired
    private UserVoucherService userVoucherService;

    @Override
    public MerchantVoucher createVoucher(AppUser merchant,
                              String voucherName,
                              String description,
                              String imgURL,
                              int stock,
                              int pointRequired,
                              LocalDate expired)
    {
        MerchantVoucher voucher = new MerchantVoucher(
                merchant,
                voucherName,
                description,
                imgURL,
                stock,
                pointRequired,
                expired);
        merchantVoucherRepository.save(voucher);
        return voucher;
    }

    @Override
    public MerchantVoucher updateVoucher(int id,
                              AppUser merchant,
                              String voucherName,
                              String description,
                              String imgURL,
                              int stock,
                              int pointRequired,
                              LocalDate expired)
    {
        MerchantVoucher voucher = new MerchantVoucher(
                merchant,
                voucherName,
                description,
                imgURL,
                stock,
                pointRequired,
                expired);
        voucher.setId(id);
        merchantVoucherRepository.save(voucher);
        return voucher;
    }

    @Override
    public Iterable<MerchantVoucher> getListMerchantVoucher(AppUser merchant){
        return merchantVoucherRepository.findByMerchant(merchant);
    }

    @Override
    public int getCountMerchantVoucherRedeemed(AppUser merchant) {
        int count = 0;
        Iterable<MerchantVoucher> merchantVoucherList = merchantVoucherRepository.findByMerchant(merchant);
        for (MerchantVoucher merchantVoucher : merchantVoucherList) {
            count += userVoucherService.getListUserVouchersByMerchantVouchers(merchantVoucher).size();
        }
        return count;
    }

    @Override
    public MerchantVoucher getVoucherById(int id){
        return merchantVoucherRepository.findById(id);
    }

}
