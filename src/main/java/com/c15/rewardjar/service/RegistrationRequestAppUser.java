package com.c15.rewardjar.service;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class RegistrationRequestAppUser {
    private final String email;
    private final String password;
    private final String password2;
    private final String firstName;
    private final String lastName;
    private final String merchantName;
    private final String phoneNumber;
}
