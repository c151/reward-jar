package com.c15.rewardjar.service;

import org.springframework.web.multipart.MultipartFile;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class RegistrationRequestMerchant {
    private final String email;
    private final String password;
    private final String merchantName;
    // private final MultipartFile profilePicture ;
    // private final byte[] profilePicture;
    private final String phoneNumber;

}
