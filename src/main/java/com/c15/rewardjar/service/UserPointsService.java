package com.c15.rewardjar.service;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.NewUserPoints;
import com.c15.rewardjar.model.UserPoints;

import java.util.List;

public interface UserPointsService {
    UserPoints createUserPoints(UserPoints userPoints);
    List<UserPoints> getListUserPoints(AppUser user);
    NewUserPoints addPointsByCode(AppUser user, String code);
    UserPoints findByUserAndMerchant(AppUser user, AppUser merchant);
    UserPoints updateUserPoints(UserPoints userPoints);
}
