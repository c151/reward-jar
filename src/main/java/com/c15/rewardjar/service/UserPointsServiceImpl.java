package com.c15.rewardjar.service;

import com.c15.rewardjar.core.point.PointAddRequest;
import com.c15.rewardjar.core.point.PointScanner;
import com.c15.rewardjar.model.*;
import com.c15.rewardjar.repository.UserPointsRepository;

import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserPointsServiceImpl implements UserPointsService {
    @Autowired
    private UserPointsRepository userPointsRepository;

    @Autowired
    private PointScanner pointScanner;

    @Override
    public UserPoints createUserPoints(UserPoints userPoints) {
        userPointsRepository.save(userPoints);
        return userPoints;
    }

    @Override
    public List<UserPoints> getListUserPoints(AppUser user) {
        return userPointsRepository.findByUser(user);
    }

    @Override
    public UserPoints updateUserPoints(UserPoints userPoints) {
        userPointsRepository.save(userPoints);
        return userPoints;
    }

    @Override
    public NewUserPoints addPointsByCode(AppUser user, String code) {
        PointAddRequest pointAddRequestResult = pointScanner.addPoints(user, code);
        if (pointAddRequestResult != null) {
            int pointAdded = pointAddRequestResult.getPoint();
            MerchantPoints merchantPoints = pointAddRequestResult.getMerchantPoints();
            AppUser merchant = merchantPoints.getMerchant();
            return new NewUserPoints(pointAdded, merchant);
        }
        return null;
    }

    @Override
    public UserPoints findByUserAndMerchant(AppUser user, AppUser merchant) {
        return userPointsRepository.findByUserAndMerchant(user, merchant);
    }
}
