package com.c15.rewardjar.service;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantVoucher;
import com.c15.rewardjar.model.UserPoints;
import com.c15.rewardjar.model.UserVoucher;

import java.util.HashMap;
import java.util.List;

public interface UserVoucherService {
    UserVoucher createUserVoucher(UserVoucher userVoucher);
    List<UserVoucher> getListUserVouchers(AppUser user);
    List<UserVoucher> getListUserVouchersByMerchantVouchers(MerchantVoucher merchantVoucher);
    HashMap<AppUser, List<UserVoucher>> getMapUserVouchers(AppUser user);
    HashMap<UserPoints, List<MerchantVoucher>> getListMerchantVouchers(List<UserPoints> userPoints);
    UserPoints getUserPointsByMerchantVoucher(AppUser user, MerchantVoucher merchantVoucher);
    UserVoucher redeemVoucher(AppUser user, MerchantVoucher merchantVoucher);
}
