package com.c15.rewardjar.service;

import com.c15.rewardjar.core.redeem.VoucherRedeemer;
import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantVoucher;
import com.c15.rewardjar.model.UserPoints;
import com.c15.rewardjar.model.UserVoucher;
import com.c15.rewardjar.repository.MerchantVoucherRepository;
import com.c15.rewardjar.repository.UserPointsRepository;
import com.c15.rewardjar.repository.UserVoucherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class UserVoucherServiceImpl implements UserVoucherService {
    @Autowired
    private UserVoucherRepository userVoucherRepository;

    @Autowired
    private UserPointsRepository userPointsRepository;

    @Autowired
    private MerchantVoucherRepository merchantVoucherRepository;

    @Override
    public UserVoucher createUserVoucher(UserVoucher userVoucher) {
        userVoucherRepository.save(userVoucher);
        return userVoucher;
    }

    @Override
    public List<UserVoucher> getListUserVouchers(AppUser user) {
        return userVoucherRepository.findByUser(user);
    }

    @Override
    public List<UserVoucher> getListUserVouchersByMerchantVouchers(MerchantVoucher merchantVoucher) {
        return userVoucherRepository.findByMerchantVoucher(merchantVoucher);
    }

    @Override
    public HashMap<AppUser, List<UserVoucher>> getMapUserVouchers(AppUser user) {
        HashMap<AppUser, List<UserVoucher>> myVouchers = new HashMap<>();
        for (UserVoucher voucher : userVoucherRepository.findByUser(user)) {
            AppUser merchant = voucher.getMerchantVoucher().getMerchant();
            List<UserVoucher> userVoucherList;
            if (!myVouchers.containsKey(merchant)) {
                userVoucherList = new ArrayList<>();
                userVoucherList.add(voucher);
                myVouchers.put(merchant, userVoucherList);
            } else {
                userVoucherList = myVouchers.get(merchant);
                userVoucherList.add(voucher);
            }
            myVouchers.put(merchant, userVoucherList);
        }
        return myVouchers;
    }

    @Override
    public UserPoints getUserPointsByMerchantVoucher(AppUser user, MerchantVoucher merchantVoucher) {
        for (UserPoints points : userPointsRepository.findByUser(user)) {
            if (points.getMerchant().equals(merchantVoucher.getMerchant())) {
                return points;
            }
        }
        return null;
    }

    @Override
    public HashMap<UserPoints, List<MerchantVoucher>> getListMerchantVouchers(List<UserPoints> userPoints) {
        HashMap<UserPoints, List<MerchantVoucher>> merchantVouchers = new HashMap<>();
        for (UserPoints userPoint : userPoints) {
            List<MerchantVoucher> vouchers = (List<MerchantVoucher>) merchantVoucherRepository.findByMerchant(userPoint.getMerchant());
            merchantVouchers.put(userPoint, vouchers);
        }
        return merchantVouchers;
    }

    @Override
    public UserVoucher redeemVoucher(AppUser user, MerchantVoucher merchantVoucher) {
        if (merchantVoucher != null) {
            UserPoints userPoints = getUserPointsByMerchantVoucher(user, merchantVoucher);
            if (userPoints != null) {
                VoucherRedeemer voucherRedeemer = new VoucherRedeemer();
                boolean redeemed = voucherRedeemer.redeemVoucher(user, merchantVoucher, userPoints);
                if (redeemed) {
                    return createUserVoucher(new UserVoucher(user, merchantVoucher));
                }
            }
        }
        return null;
    }
}
