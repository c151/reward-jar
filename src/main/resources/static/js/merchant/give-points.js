class NumpadButton {
    constructor(numpadButton){
        this.numpadButton = numpadButton;
    }

    get innerText(){
        return this.numpadButton.innerText;
    }

    get type(){
        return this.numpadButton.getAttribute("data-type");
    }

    click(isClicked){
        if(isClicked){
            this.numpadButton.classList.add("bg-gray");
        }
        else{
            this.numpadButton.classList.remove("bg-gray");
        }
    }

    addEventListener(type, listener){
        this.numpadButton.addEventListener(type, listener);
    }
}

function initPointsGiven(){
    pointsGiven = document.querySelector(".merchant-give-points-output-value");
    hiddenPointsGiven = document.getElementById("hidden-give-points-input");
}

class NumpadButtonListener {
    constructor(numpadButton){
        this.numpadButton = numpadButton;
    }

    updatePointsGiven(){
    }

    updateHiddenPointsGiven(){
        hiddenPointsGiven.value = parseInt(pointsGiven.innerText);
    }

    handleEvent(){
        this.updatePointsGiven();
        this.updateHiddenPointsGiven();
    }
}

class NumpadDigitButtonListener extends NumpadButtonListener {
    constructor(numpadButton){
        super(numpadButton);
    }

    updatePointsGiven(){
        if(pointsGiven.innerText == "0"){
            pointsGiven.innerText = this.numpadButton.innerText;
        }
        else{
            pointsGiven.innerText += this.numpadButton.innerText;
        }
    }
}

class NumpadClearButtonListener extends NumpadButtonListener {
    updatePointsGiven(){
        pointsGiven.innerText = "0";
    }
}

class NumpadBackspaceButtonListener extends NumpadButtonListener {
    updatePointsGiven(){
        if(pointsGiven.innerText.length <= 1){
            pointsGiven.innerText = "0";
        }
        else{
            pointsGiven.innerText = pointsGiven.innerText.substring(0, pointsGiven.innerText.length - 1);
        }
    }
}

function initNumpadButtons(){
    let numpadButtons = document.querySelector(".merchant-give-points-numpad")
                                .querySelectorAll(".button");
    for(let i = 0; i < numpadButtons.length; i++){
        let numpadButton = new NumpadButton(numpadButtons[i]);
        let numpadButtonListener = null;

        if(numpadButton.type == "digit"){
            numpadButtonListener = new NumpadDigitButtonListener(numpadButton);
        }
        else if(numpadButton.type == "clear"){
            numpadButtonListener = new NumpadClearButtonListener();
        }
        else if(numpadButton.type == "backspace"){
            numpadButtonListener = new NumpadBackspaceButtonListener();
        }

        numpadButton.addEventListener("click", numpadButtonListener);
        numpadButton.addEventListener("touchstart", () => numpadButton.click(true));
        numpadButton.addEventListener("touchend", () => numpadButton.click(false));
    }
}

var pointsGiven;
var pointsGivenValue;
var hiddenPointsGiven;
$(document).ready(function(){
    initPointsGiven();
    initNumpadButtons();
});