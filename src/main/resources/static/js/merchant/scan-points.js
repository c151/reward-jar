function waitQRCodeImageFullyLoaded(){
    qrCodeImage = new Image();
    qrCodeImage.addEventListener("load", () => {
        pollInterval = setInterval(checkMerchantPointsValidity, 400);
        document.querySelector(".loading-overlay").replaceWith(qrCodeImage);
    });
    qrCodeImage.src = "https://chart.googleapis.com/chart?cht=qr&chs=225x225&chld=H|1&chl=" + qrCode;
}

function checkMerchantPointsValidity(){
    $.ajax({
        url: "points-api/" + qrCode,
        dataType: "json",
        type: "GET",
        success: function(data){
            if(data.isUsed){
                let successIcon = document.createElement("i");
                successIcon.classList.add("fas", "fa-check-square", "merchant-scan-points-success-icon");
                qrCodeImage.replaceWith(successIcon);
                document.querySelector(".merchant-give-points-qr-code-text").innerText = "Success!";
                clearInterval(pollInterval);
            }
        },
        error: function(){
          console.log("Error!");
        }
    });
}

var qrCode;
var qrCodeImage;
var pollInterval;
$(document).ready(function(){
    qrCode = document.getElementById("hidden-qr-code").value;
    waitQRCodeImageFullyLoaded();
});