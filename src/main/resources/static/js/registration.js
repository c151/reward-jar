/**
 * Function to be executed when Register button is pressed
 *
 * @return none
 */
function thing() {
  // PREPARE FORM DATA
  var x = $("form").serializeArray();
  var curURL = window.location.pathname + window.location.search;
  var ajaxURL = "/api" + curURL;
  var formData = {};
  $.each(x, function (_, field) {
    $("#output").append(field.name + ":" + field.value + " ");
    formData[field.name] = field.value;
  });

  // DO POST
  $.ajax({
    type: "POST",
    contentType: "application/json",
    url: ajaxURL,
    dataType: "application/json",
    data: JSON.stringify(formData),
    cache: false,
    success: function (response) {
      alert("Something!!");
    },
    error: function (e) {
      alert("Error!");
      console.log("ERROR: ", e);
    },
  });
    resetData();
}


function resetData() {
  $.each(x, function (_, field) {
    $("#output").append(field.name + ":" + field.value + " ");
    formData[field.name] = "";
  });
}
