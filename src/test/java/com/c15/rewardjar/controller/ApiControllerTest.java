package com.c15.rewardjar.controller;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.AppUserRole;
import com.c15.rewardjar.repository.AppUserRepository;
import com.c15.rewardjar.service.AppUserServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(controllers = ApiController.class)
public class ApiControllerTest {

	private MockMvc mockMvc;
	@MockBean
	private AppUserRepository appUserRepository;

	@MockBean
	private AppUserServiceImpl appUserService;

	@MockBean
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private ObjectMapper objectMapper;

	private AppUser appUserBefore;
	private AppUser appMerchantBefore;
	private AppUser testUser;

	@BeforeEach
	public void setup() throws Exception {
		appUserBefore = new AppUser();
		appUserBefore.setEmail("lala@lala.com");
		appUserBefore.setPassword("pass");
		appUserBefore.setFirstName("last");
		appUserBefore.setLastName("first");
		appUserBefore.setPhoneNumber("312789231");
		appUserBefore.setAppUserRole(AppUserRole.USER);
		appUserBefore.setIsVerified(true);
		appUserBefore.setPoint(0L);
		appUserBefore.setLocked(false);
		appUserBefore.setEnabled(true);

		appMerchantBefore = new AppUser();
		appMerchantBefore.setEmail("m@lala.com");
		appMerchantBefore.setPassword("pass");
		appMerchantBefore.setPhoneNumber("12789231");
		appMerchantBefore.setMerchantName("lmao");
		appMerchantBefore.setAppUserRole(AppUserRole.MERCHANT);
		appMerchantBefore.setIsVerified(true);
		appMerchantBefore.setPoint(0L);
		appMerchantBefore.setLocked(false);
		appMerchantBefore.setEnabled(true);

		testUser = new AppUser();
		testUser.setEmail("test@lala.com");
		testUser.setPassword("pass");
		testUser.setFirstName("test");
		testUser.setLastName("test");
		testUser.setPhoneNumber("3789231");
		testUser.setAppUserRole(AppUserRole.USER);
		testUser.setIsVerified(true);
		testUser.setPoint(0L);
		testUser.setLocked(false);
		testUser.setEnabled(true);

		mockMvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}

	@Test
	public void registerUserTest() throws Exception {
		this.mockMvc.perform(post("/api/user/register").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(appUserBefore))).andExpect(status().isCreated());
	}

	@Test
	public void getUserTestByEmail() throws Exception {
		this.mockMvc.perform(get("/api/user/{email}", testUser.getEmail())).andExpect(status().isOk());
	}

	@Test
	public void deleteUserTestByEmail() throws Exception {
		when(appUserService.getUserByEmail(Mockito.any())).thenReturn(appUserBefore);
		this.mockMvc.perform(delete("/api/user/{email}", "lala@.lala.com").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void deleteUserTestByEmailAppUserNull() throws Exception {
		when(appUserService.getUserByEmail(Mockito.any())).thenReturn(null);
		assertThrows(NestedServletException.class, () -> {
			this.mockMvc.perform(delete("/api/user/{email}", "lala@.lala.com").contentType(MediaType.APPLICATION_JSON));
		});
	}

	@Test
	public void registerMerchantTest() throws Exception {
		this.mockMvc.perform(post("/api/merchant/register").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(appUserBefore))).andExpect(status().isCreated());
	}

	@Test
	public void getMerchantTestByEmail() throws Exception {
		this.mockMvc.perform(get("/api/merchant/email/{email}", appMerchantBefore.getEmail()))
				.andExpect(status().isOk());
	}

	@Test
	public void getMerchantTestByMerchantName() throws Exception {
		this.mockMvc.perform(get("/api/merchant/{merchantName}", appMerchantBefore.getMerchantName()))
				.andExpect(status().isOk());
	}

	@Test
	public void deleteMerchantTestByEmail() throws Exception {
		when(appUserService.getUserByEmail(Mockito.any())).thenReturn(appUserBefore);
		this.mockMvc
				.perform(delete("/api/merchant/email/{email}", "lala@lala.com").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void deleteMerchantTestByEmailAppUserNull() throws Exception {
		when(appUserService.getUserByEmail(Mockito.any())).thenReturn(null);
		assertThrows(NestedServletException.class, () -> {
			this.mockMvc.perform(delete("/api/merchant/email/{email}", "lala@lala.com").contentType(MediaType.APPLICATION_JSON));
		});
	}

	@Test
	public void deleteMerchantTestByMerchantName() throws Exception {
		when(appUserService.getUserByMerchantName(Mockito.any())).thenReturn(appMerchantBefore);
		this.mockMvc.perform(delete("/api/merchant/{merchantName}", "lmao").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is2xxSuccessful());
	}

	@Test
	public void deleteMerchantTestByMerchantNameAppUserNull() throws Exception {
		when(appUserService.getUserByMerchantName(Mockito.any())).thenReturn(null);
		assertThrows(NestedServletException.class, () -> {
			this.mockMvc.perform(delete("/api/merchant/{merchantName}", "lmao").contentType(MediaType.APPLICATION_JSON));
		});
	}
}
