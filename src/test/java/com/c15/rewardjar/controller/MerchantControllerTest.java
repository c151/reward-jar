package com.c15.rewardjar.controller;

import com.c15.rewardjar.core.fileuploader.FileUpload;
import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.AppUserRole;
import com.c15.rewardjar.model.MerchantPoints;
import com.c15.rewardjar.model.MerchantVoucher;
import com.c15.rewardjar.repository.AppUserRepository;
import com.c15.rewardjar.repository.MerchantPointRepository;
import com.c15.rewardjar.repository.MerchantVoucherRepository;
import com.c15.rewardjar.service.AppUserServiceImpl;
import com.c15.rewardjar.service.MerchantPointServiceImpl;
import com.c15.rewardjar.service.MerchantVoucherServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.time.LocalDate;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = MerchantController.class)
public class MerchantControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MerchantPointServiceImpl merchantPointService;

    @MockBean
    private MerchantPointRepository merchantPointRepository;

    @MockBean
    private MerchantVoucherRepository merchantVoucherRepository;

    @MockBean
    private MerchantVoucherServiceImpl merchantVoucherService;

    @MockBean
    private AppUserRepository appUserRepository;

    @MockBean
    private AppUserServiceImpl appUserService;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private AppUser merchant;
    private AppUser merchant2;
    private MerchantPoints merchantPoints;
    private File mockFile;
    private MerchantVoucher merchantVoucher;
    private MerchantVoucher merchantVoucher2;

    @BeforeEach
    public void setUp() throws Exception{
        merchant = new AppUser();
        merchant.setMerchantName("Test Merchant");
        merchant.setAppUserRole(AppUserRole.MERCHANT);

        merchant2 = new AppUser();
        merchant2.setMerchantName("Test Merchant2");
        merchant2.setId(999l);
        merchant2.setAppUserRole(AppUserRole.MERCHANT);

        merchantPoints = new MerchantPoints();
        merchantPoints.setMerchant(merchant);
        merchantPoints.setPoints(1);
        merchantPoints.setQRCode("QRCODE");
        merchantPoints.setUsed(false);

        mockFile = new File("testFile.txt");
        mockFile.createNewFile();

        merchantVoucher = new MerchantVoucher();
        merchantVoucher.setMerchant(merchant);
        merchantVoucher.setVoucherName("McDonalds Notebook");
        merchantVoucher.setDescription("Ini voucher");
        merchantVoucher.setStock(90);
        merchantVoucher.setPointRequired(200);
        merchantVoucher2 = new MerchantVoucher();
    }

    @Test
    @WithMockUser
    public void whenMerchantURLIsAccessedByUserItShouldReturnForbidden() throws Exception {
        mockMvc.perform(get("/merchant/"))
               .andExpect(status().isForbidden());
    }

    @Test
    @WithAnonymousUser
    public void whenMerchantURLIsAccessedByAnonymousUserItShouldReturn3xxRedirection() throws Exception {
        mockMvc.perform(get("/merchant/"))
               .andExpect(status().is3xxRedirection());
    }

    @Test
    public void whenMerchantIndexURLIsAccessedByMerchantItShouldReturnMerchantIndex() throws Exception {
        when(merchantPointService.getTotalPointsShared(merchant)).thenReturn(5);
        mockMvc.perform(get("/merchant/").with(user(merchant)))
               .andExpect(status().isOk())
               .andExpect(handler().methodName("index"))
               .andExpect(model().attribute("pointsShared", equalTo(5)))
               .andExpect(model().attribute("headerUrl", equalTo("/image/header-xl.png")))
               .andExpect(view().name("merchant/index"));
    }

    @Test
    public void whenMerchantGivePointsURLIsAccessedByMerchantItShouldReturnMerchantPointsGive() throws Exception {
        mockMvc.perform(get("/merchant/give-points").with(user(merchant)))
               .andExpect(status().isOk())
               .andExpect(handler().methodName("givePoints"))
               .andExpect(view().name("merchant/point/give"));
    }

    @Test
    public void testControllerListVoucher() throws Exception {
        mockMvc.perform(get("/merchant/list-voucher").with(user(merchant)))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("viewVoucher"))
                .andExpect(view().name("merchant/voucher/list"));
    }

    @Test
    public void testControllerCreateVoucher() throws Exception {
        mockMvc.perform(get("/merchant/create-voucher").with(user(merchant)))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("createVoucher"))
                .andExpect(view().name("merchant/voucher/create"));
    }

    @Test
    public void testControllerCreateVoucherApi() throws Exception {
        mockMvc.perform(post("/merchant/create-voucher-api").with(user(merchant))
                .param("voucherName", "test")
                .param("description", "tester")
                .param("stock", "1")
                .param("pointRequired", "1")
                .param("expiredDate", "2017-01-13"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("createVoucherAPI"));
    }

    @Test
    public void testControllerUpdateVoucher() throws Exception {
        when(merchantVoucherService.getVoucherById(0)).thenReturn(merchantVoucher);
        mockMvc.perform(get("/merchant/update-voucher/0").with(user(merchant)))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("updateVoucher"))
                .andExpect(view().name("merchant/voucher/update"));
    }

    @Test
    public void testControllerUpdateVoucherWhenVoucherIsNotFound() throws Exception {
        when(merchantVoucherService.getVoucherById(0)).thenReturn(null);
        mockMvc.perform(get("/merchant/update-voucher/0").with(user(merchant)))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("updateVoucher"))
                .andExpect(view().name("redirect:/merchant/list-voucher"));
    }

    @Test
    public void testControllerUpdateVoucherWhenMerchantIsNotMatch() throws Exception {
        when(merchantVoucherService.getVoucherById(0)).thenReturn(merchantVoucher);
        mockMvc.perform(get("/merchant/update-voucher/0").with(user(merchant2)))
                .andExpect(status().is3xxRedirection())
                .andExpect(handler().methodName("updateVoucher"))
                .andExpect(view().name("redirect:/merchant/list-voucher"));
    }

    @Test
    public void testControllerUpdateVoucherApi() throws Exception {
        when(merchantVoucherService.getVoucherById(0)).thenReturn(merchantVoucher);
        mockMvc.perform(post("/merchant/update-voucher-api").with(user(merchant))
                .param("voucherName", "test")
                .param("description", "tester")
                .param("imgURL", "https://img.text")
                .param("stock", "1")
                .param("pointRequired", "1")
                .param("id", "0")
                .param("expiredDate", "2017-01-13"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("updateVoucherAPI"));
    }

    @Test
    public void testControllerUpdateVoucherApiWhenVoucherIsNotFound() throws Exception {
        when(merchantVoucherService.getVoucherById(0)).thenReturn(merchantVoucher);
        mockMvc.perform(post("/merchant/update-voucher-api").with(user(merchant))
                .param("voucherName", "test")
                .param("description", "tester")
                .param("imgURL", "https://img.text")
                .param("stock", "1")
                .param("pointRequired", "1")
                .param("id", "1")
                .param("expiredDate", "2017-01-13"))
                .andExpect(status().isForbidden())
                .andExpect(handler().methodName("updateVoucherAPI"));
    }

    @Test
    public void testControllerUpdateVoucherApiWhenMerchantIsNotMatch() throws Exception {
        when(merchantVoucherService.getVoucherById(0)).thenReturn(merchantVoucher);
        mockMvc.perform(post("/merchant/update-voucher-api").with(user(merchant2))
                .param("voucherName", "test")
                .param("description", "tester")
                .param("imgURL", "https://img.text")
                .param("stock", "1")
                .param("pointRequired", "1")
                .param("id", "0")
                .param("expiredDate", "2017-01-13"))
                .andExpect(status().isForbidden())
                .andExpect(handler().methodName("updateVoucherAPI"));
    }


    @Test
    public void whenMerchantGivePointsURLIsAccessedByMerchantItShouldReturnMerchantPointsScan() throws Exception {
        when(merchantPointService.createMerchantPoints(merchant, 1)).thenReturn(merchantPoints);
        mockMvc.perform(post("/merchant/scan-points").with(user(merchant)).param("points-given", "1"))
               .andExpect(status().isOk())
               .andExpect(handler().methodName("scanPoints"))
               .andExpect(model().attribute("pointsGiven", equalTo(1)))
               .andExpect(model().attribute("qrCode", equalTo("QRCODE")))
               .andExpect(view().name("merchant/point/scan"));
    }

    @Test
    public void whenGetMerchantPointsIsAccessedByMerchantItShouldReturnMerchantPointsJSON() throws Exception {
        when(merchantPointService.getMerchantPointsByQRCode("QRCODE")).thenReturn(merchantPoints);
        mockMvc.perform(get("/merchant/points-api/QRCODE").with(user(merchant)))
               .andExpect(status().isOk())
               .andExpect(handler().methodName("getMerchantPoints"))
               .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
               .andExpect(jsonPath("$.qrCode", is("QRCODE")))
               .andExpect(jsonPath("$.merchantName", is("Test Merchant")))
               .andExpect(jsonPath("$.points", is(1)))
               .andExpect(jsonPath("$.isUsed", is(false)));
    }

    @Test
    public void testUpdateProfilePage() throws Exception {
        mockMvc.perform(get("/merchant/profile").with(user(merchant)))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("updateProfile"))
                .andExpect(view().name("merchant/profile"));
    }

    @Test
    public void testSetVoucherPicture() throws Exception {
        MockedStatic<?> mockedStatic = mockStatic(FileUpload.class);
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.txt",
                "text/plain", "Test Spring Framework".getBytes());
        mockedStatic.when(() -> FileUpload.uploadFileToService(any())).thenReturn("urlResult");
        mockedStatic.when(() -> FileUpload.convertMultiPartToFile(any())).thenReturn(mockFile);
        when(merchantVoucherService.getVoucherById(anyInt())).thenReturn(merchantVoucher2);

        mockMvc.perform(multipart("/merchant/set-voucher-picture/1").file(multipartFile).with(user(merchant)))
                .andExpect(status().isOk());
        mockedStatic.close();

    }

    @Test
    public void testSetProfilePicture() throws Exception {
        MockedStatic<?> mockedStatic = mockStatic(FileUpload.class);
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.txt",
                "text/plain", "Test Spring Framework".getBytes());
        mockedStatic.when(() -> FileUpload.uploadFileToService(any())).thenReturn("urlResult");
        mockedStatic.when(() -> FileUpload.convertMultiPartToFile(any())).thenReturn(mockFile);

        mockMvc.perform(multipart("/merchant/set-profile-picture").file(multipartFile).with(user(merchant)))
                .andExpect(status().isOk());
        mockedStatic.close();
    }
}
