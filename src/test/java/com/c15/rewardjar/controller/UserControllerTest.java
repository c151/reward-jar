package com.c15.rewardjar.controller;

import com.c15.rewardjar.core.fileuploader.FileUpload;
import com.c15.rewardjar.model.*;
import com.c15.rewardjar.repository.AppUserRepository;
import com.c15.rewardjar.repository.UserVoucherRepository;
import com.c15.rewardjar.service.*;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@WebMvcTest(controllers = UserController.class)
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserPointsService userPointsService;

    @MockBean
    private UserVoucherService userVoucherService;

    @MockBean
    private AppUserRepository appUserRepository;

    @MockBean
    private MerchantPointService merchantPointService;

    @MockBean
    private MerchantVoucherService merchantVoucherService;

    @MockBean
    private UserVoucherRepository userVoucherRepository;

    @MockBean
    private AppUserServiceImpl appUserService;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @MockBean
    private FileUpload fileUpload;

    private AppUser user;
    private AppUser merchant;
    private NewUserPoints newUserPoints;
    private File mockFile;

    private UserVoucher userVoucher;
    private MerchantVoucher merchantVoucher;
    private UserPoints userPoints;

    @BeforeEach
    public void setUp() throws Exception{
        user = new AppUser();
        user.setAppUserRole(AppUserRole.USER);
        merchant = new AppUser();
        newUserPoints = new NewUserPoints(100, null);
        mockFile = new File("testFile.txt");
        mockFile.createNewFile();

        userVoucher = new UserVoucher();
        merchantVoucher = new MerchantVoucher();
        userPoints = new UserPoints();
        merchantVoucher.setMerchant(merchant);
        merchantVoucher.setVoucherName("McDonalds Notebook");
        merchantVoucher.setDescription("Ini voucher");
        merchantVoucher.setStock(90);
        merchantVoucher.setPointRequired(200);
        userVoucher.setId(0);
        userVoucher.setMerchantVoucher(merchantVoucher);
    }

    @Test
    public void testUserIndex() throws Exception {
        mockMvc.perform(get("/user/").with(user(user)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(handler().methodName("index"))
                .andExpect(view().name("user/index"));

        verify(userPointsService, times(1)).getListUserPoints(any());
        verify(userVoucherService, times(1)).getListUserVouchers(any());
    }

    @Test
    public void testScanPointsHomePage() throws Exception {
        mockMvc.perform(get("/user/scan-points").with(user(user)))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("scanPoints"))
                .andExpect(view().name("user/scan"));
    }

    @Test
    public void testUpdateProfilePage() throws Exception {
        mockMvc.perform(get("/user/profile").with(user(user)))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("updateProfile"))
                .andExpect(view().name("user/profile"));
    }

    @Test
    public void testScanPointsAPI() throws Exception {
        lenient().when(userPointsService.addPointsByCode(any(), eq("abcde"))).thenReturn(newUserPoints);
        mockMvc.perform(post("/user/scan-points-api").with(user(user)).param("code", "abcde"))
                .andExpect(status().isOk())
                .andExpect(handler().methodName("scanPointAPI"))
                .andExpect(jsonPath("$.points").value(100));
    }

    @Test
    public void testScanPointsAPICodeNotFound() throws Exception {
        lenient().when(userPointsService.addPointsByCode(any(), eq("abcde"))).thenReturn(null);
        mockMvc.perform(post("/user/scan-points-api").with(user(user)).param("code", "abcde"))
                .andExpect(status().isBadRequest())
                .andExpect(handler().methodName("scanPointAPI"));
    }

    @Test
    public void testSetProfilePicture() throws Exception {
        MockedStatic<?> mockedStatic = mockStatic(FileUpload.class);
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.txt",
                "text/plain", "Test Spring Framework".getBytes());
        mockedStatic.when(() -> FileUpload.uploadFileToService(any())).thenReturn("urlResult");
        mockedStatic.when(() -> FileUpload.convertMultiPartToFile(any())).thenReturn(mockFile);

        mockMvc.perform(multipart("/user/set-profile-picture").file(multipartFile).with(user(user)))
                .andExpect(status().isOk());
        mockedStatic.close();
    }

    @Test
    public void testSetProfilePictureCallRealMethod() throws Exception {
        MockedStatic<?> mockedStatic = mockStatic(FileUpload.class);
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.txt",
                "text/plain", "Spring Framework".getBytes());
        mockedStatic.when(() -> FileUpload.uploadFileToService(any())).thenReturn("urlResult");
        mockedStatic.when(() -> FileUpload.convertMultiPartToFile(any())).thenCallRealMethod();

        mockMvc.perform(multipart("/user/set-profile-picture").file(multipartFile).with(user(user)))
                .andExpect(status().isOk());

        mockedStatic.close();
    }

    @Test
    public void testGetUserVouchers() throws Exception {
        mockMvc.perform(get("/user/my-voucher").with(user(user)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getUserVouchers"))
                .andExpect(view().name("user/myvoucher"));

        verify(userVoucherService, times(1)).getMapUserVouchers(any());
    }

    @Test
    public void testGetUserVoucherDetails() throws Exception {
        when(userVoucherRepository.findById(0)).thenReturn(userVoucher);
        mockMvc.perform(get("/user/my-voucher/0").with(user(user)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getUserVoucherDetails"))
                .andExpect(view().name("user/myvoucherdetails"));
    }

    @Test
    public void testRedeemCatalog() throws Exception {
        when(userVoucherRepository.findById(0)).thenReturn(userVoucher);
        mockMvc.perform(get("/user/catalog").with(user(user)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(handler().methodName("redeemCatalog"))
                .andExpect(view().name("user/redeemcatalog"));

        verify(userVoucherService, times(1)).getListMerchantVouchers(any());
    }

    @Test
    public void testGetVouchersDetails() throws Exception {
        merchant.setMerchantName("Traveloka");
        List<MerchantVoucher> merchantVoucherList = new ArrayList<>();
        merchantVoucherList.add(merchantVoucher);
        Optional<AppUser> optMerchant = Optional.of(merchant);
        when(appUserRepository.findByMerchantName("Traveloka")).thenReturn(optMerchant);
        when(userVoucherRepository.findById(0)).thenReturn(userVoucher);
        when(merchantVoucherService.getListMerchantVoucher(merchant)).thenReturn(merchantVoucherList);
        when(userPointsService.findByUserAndMerchant(user, merchant)).thenReturn(userPoints);
        mockMvc.perform(get("/user/catalog/Traveloka").with(user(user)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getVouchersDetails"))
                .andExpect(view().name("user/merchantvouchers"));

        verify(merchantVoucherService, times(1)).getListMerchantVoucher(any());
        verify(userPointsService, times(1)).findByUserAndMerchant(any(), any());
    }

    @Test
    public void testGetVoucherDetails() throws Exception {
        merchant.setMerchantName("Traveloka");
        List<MerchantVoucher> merchantVoucherList = new ArrayList<>();
        merchantVoucherList.add(merchantVoucher);
        Optional<AppUser> optMerchant = Optional.of(merchant);
        when(appUserRepository.findByMerchantName("Traveloka")).thenReturn(optMerchant);
        when(userVoucherRepository.findById(0)).thenReturn(userVoucher);
        when(merchantVoucherService.getListMerchantVoucher(merchant)).thenReturn(merchantVoucherList);
        when(userPointsService.findByUserAndMerchant(user, merchant)).thenReturn(userPoints);
        when(merchantVoucherService.getVoucherById(0)).thenReturn(merchantVoucher);
        mockMvc.perform(get("/user/catalog/Traveloka/0").with(user(user)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(handler().methodName("getVoucherDetails"))
                .andExpect(view().name("user/voucherdetails"));

        verify(merchantVoucherService, times(1)).getVoucherById(0);
        verify(userPointsService, times(1)).findByUserAndMerchant(any(), any());
    }

    @Test
    public void testRedeemVoucher() throws Exception {
        merchant.setMerchantName("Traveloka");
        when(merchantVoucherService.getVoucherById(0)).thenReturn(merchantVoucher);
        when(userVoucherService.redeemVoucher(user, merchantVoucher)).thenReturn(userVoucher);
        when(userVoucherRepository.findById(0)).thenReturn(userVoucher);
        when(userPointsService.findByUserAndMerchant(user, merchant)).thenReturn(userPoints);
        mockMvc.perform(get("/user/catalog/Traveloka/0/redeem").with(user(user)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(handler().methodName("redeemVoucher"))
                .andExpect(view().name("user/voucherredeemed"));

        verify(merchantVoucherService, times(1)).getVoucherById(0);
        verify(userVoucherService, times(1)).redeemVoucher(any(), any());
        verify(userPointsService, times(1)).findByUserAndMerchant(any(), any());
    }

    @Test
    public void testRedeemVoucherFail() throws Exception {
        merchant.setMerchantName("Traveloka");
        when(merchantVoucherService.getVoucherById(0)).thenReturn(merchantVoucher);
        when(userVoucherService.redeemVoucher(user, merchantVoucher)).thenReturn(null);
        when(userVoucherRepository.findById(0)).thenReturn(userVoucher);
        when(userPointsService.findByUserAndMerchant(user, merchant)).thenReturn(userPoints);
        mockMvc.perform(get("/user/catalog/Traveloka/0/redeem").with(user(user)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(handler().methodName("redeemVoucher"))
                .andExpect(view().name("user/vouchernotredeemed"));

        verify(merchantVoucherService, times(1)).getVoucherById(0);
        verify(userVoucherService, times(1)).redeemVoucher(any(), any());
        verify(userPointsService, times(1)).findByUserAndMerchant(any(), any());
    }
}
