package com.c15.rewardjar.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.AppUserRole;
import com.c15.rewardjar.repository.AppUserRepository;
import com.c15.rewardjar.repository.MerchantPointRepository;
import com.c15.rewardjar.service.AppUserServiceImpl;
import com.c15.rewardjar.service.MerchantPointServiceImpl;
import com.c15.rewardjar.service.MerchantVoucherServiceImpl;
import com.c15.rewardjar.service.RegistrationRequestAppUser;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = WebPageController.class)
public class WebPageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MerchantPointServiceImpl merchantPointService;

    @MockBean
    private MerchantPointRepository merchantPointRepository;

    @MockBean
    private MerchantVoucherServiceImpl merchantVoucherService;

    @MockBean
    private AppUserRepository appUserRepository;

    @MockBean
    private AppUserServiceImpl appUserService;

    @MockBean
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private AppUser merchant;
    private AppUser user;


   @BeforeEach
    public void setUp(){
        user = new AppUser();
        user.setAppUserRole(AppUserRole.USER);
        merchant = new AppUser();
        merchant.setAppUserRole(AppUserRole.MERCHANT);

    }

    @Test
    public void accessLoginPageTest() throws Exception {
        this.mockMvc.perform(get("/login")).andExpect(status().isOk());
    }

    @Test
    public void accessUserLoginPageTest() throws Exception {
       this.mockMvc.perform(get("/user/login")).andExpect(status().isOk());
    }

    @Test
    public void accessMerchantLoginPageTest() throws Exception {
       this.mockMvc.perform(get("/merchant/login")).andExpect(status().isOk());
    }

    @Test
    public void accessUserRegisterPageTest() throws Exception {
        this.mockMvc.perform(get("/user/register")).andExpect(status().isOk());
    }

    @Test
    public void accessMerchantRegisterPageTest() throws Exception {
        this.mockMvc.perform(get("/merchant/register")).andExpect(status().isOk());
    }

    @Test
    public void redirectUserRegisterPageTest() throws Exception {
        String email = "laa@lla.com";
        String password = "pass";
        String password2 = "pass";
        String firstName = "lst";
        String lastName = "fist";
        String merchantName = "";
        String phoneNumber = "038912";

        RegistrationRequestAppUser randomObj = new RegistrationRequestAppUser(email, password, password2, firstName,
                lastName, merchantName, phoneNumber) {
        };
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(randomObj);
        this.mockMvc.perform(post("/user/register").contentType(MediaType.APPLICATION_JSON).content(json)
                .characterEncoding("utf-8").accept(MediaType.APPLICATION_JSON));
    }

    @Test
    public void redirectMerchantRegisterPageTest() throws Exception {
        String email = "laa@lla.com";
        String password = "pass";
        String password2 = "pass";
        String firstName = "lst";
        String lastName = "fist";
        String merchantName = "lmao";
        String phoneNumber = "038912";

        RegistrationRequestAppUser randomObj = new RegistrationRequestAppUser(email, password, password2, firstName,
                lastName, merchantName, phoneNumber) {
        };
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(randomObj);
        this.mockMvc.perform(post("/merchant/register").contentType(MediaType.APPLICATION_JSON).content(json)
                .characterEncoding("utf-8").accept(MediaType.APPLICATION_JSON));
    }

    @Test
    public void indexPageTest() throws Exception{
        this.mockMvc.perform(get("/")).andExpect(status().isOk());
    }

    @Test
    public void indexPageUserTest() throws Exception{
        this.mockMvc.perform(get("/").with(user(user))).andExpect(status().is3xxRedirection());
    }
    @Test
    public void indexPageMerchantTest() throws Exception{
        this.mockMvc.perform(get("/").with(user(merchant))).andExpect(status().is3xxRedirection());
    }

}
