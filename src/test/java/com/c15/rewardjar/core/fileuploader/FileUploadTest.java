package com.c15.rewardjar.core.fileuploader;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.mock.web.MockMultipartFile;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

public class FileUploadTest {
    @InjectMocks
    private FileUpload fileUpload;

    @Test
    public void testUploadFile() throws Exception {
        File testFile = File.createTempFile("testFileUpload",".txt");
        testFile.deleteOnExit();
        String fileUrl = FileUpload.uploadFileToService(testFile);
        assertNotEquals("", fileUrl);
        assertNotNull(fileUrl);
    }

    @Test
    public void testMultiPartFileConverter() throws Exception {
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test.txt",
"text/plain", "Spring Framework".getBytes());
        File resultFile = FileUpload.convertMultiPartToFile(multipartFile);
        assertNotNull(resultFile);
    }
}
