package com.c15.rewardjar.core.point;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

public class AbstractPointScanHandlerTest {
    private Class<?> abstractPointScanHandlerClass;

    @BeforeEach
    public void setUp() throws Exception {
        abstractPointScanHandlerClass = Class.forName("com.c15.rewardjar.core.point.AbstractPointScanHandler");
    }

    @Test
    public void testAbstractPointScanHandlerIsAbstractClass() throws  Exception {
        assertTrue(Modifier.isAbstract(abstractPointScanHandlerClass.getModifiers()));
    }

    @Test
    public void testSetNextMethod() throws Exception {
        Class[] cArg = new Class[1];
        cArg[0] = AbstractPointScanHandler.class;
        Method setNextMethod = abstractPointScanHandlerClass.getDeclaredMethod("setNext", cArg);
        int methodModifiers = setNextMethod.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testHandleMethod() throws Exception {
        Class[] cArg = new Class[1];
        cArg[0] = PointAddRequest.class;
        Method handleMethod = abstractPointScanHandlerClass.getDeclaredMethod("handle", cArg);
        int methodModifiers = handleMethod.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
    }
}
