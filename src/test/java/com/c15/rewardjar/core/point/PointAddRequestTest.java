package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantPoints;
import com.c15.rewardjar.model.NewUserPoints;
import com.c15.rewardjar.model.UserPoints;
import org.codehaus.groovy.runtime.MethodRankHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Assertions;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class PointAddRequestTest {
    private PointAddRequest pointAddRequest;
    private AppUser appUser;
    private UserPoints userPoints;
    private MerchantPoints merchantPoints;

    @BeforeEach
    public void setUp() {
        appUser = new AppUser();
        userPoints = new UserPoints();
        merchantPoints = new MerchantPoints();
        pointAddRequest = new PointAddRequest(appUser, "abcde");
    }

    @Test
    public void testGetUserReturnAppUser() throws Exception {
        AppUser resultAppUser = pointAddRequest.getUser();
        assertEquals(appUser, resultAppUser);
    }

    @Test
    public void testGetCodeReturnCode() throws Exception {
        String resultCode = pointAddRequest.getCode();
        assertEquals("abcde", resultCode);
    }

    @Test
    public void testGetUserPoints() throws Exception {
        pointAddRequest.setUserPoints(userPoints);
        UserPoints resultUserPoints = pointAddRequest.getUserPoints();
        assertEquals(userPoints, resultUserPoints);
    }

    @Test
    public void testGetPoints() throws Exception {
        pointAddRequest.setPoint(100);
        int resultPoint = pointAddRequest.getPoint();
        assertEquals(100, resultPoint);
    }

    @Test
    public void testGetMerchantPoints() throws Exception {
        pointAddRequest.setMerchantPoints(merchantPoints);
        MerchantPoints resultMerchantPoints = pointAddRequest.getMerchantPoints();
        assertEquals(merchantPoints, resultMerchantPoints);
    }
}
