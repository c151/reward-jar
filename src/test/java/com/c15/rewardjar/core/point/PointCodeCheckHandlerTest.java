package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantPoints;
import com.c15.rewardjar.service.MerchantPointService;
import org.codehaus.groovy.runtime.MethodRankHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Assertions;


import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class PointCodeCheckHandlerTest {
    @InjectMocks
    private PointCodeCheckHandler pointCodeCheckHandler;

    @Mock
    private MerchantPointService merchantPointService;

    @Mock
    private MerchantPoints merchantPoints;

    private Class<?> pointCodeCheckHandlerClass;
    private PointAddRequest pointAddRequest;
    private AppUser appUser;
    private MerchantPoints mockMerchantPoints;

    @BeforeEach
    public void setUp() throws Exception {
        pointCodeCheckHandlerClass = Class.forName("com.c15.rewardjar.core.point.PointCodeCheckHandler");
        appUser = new AppUser();
        mockMerchantPoints = new MerchantPoints();
        mockMerchantPoints.setPoints(100);
        pointAddRequest = new PointAddRequest(appUser, "abcde");
    }

    @Test
    public void testPointCodeCheckHandlerClassIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(pointCodeCheckHandlerClass.getModifiers()));
    }

    @Test
    public void testPointCodeCheckHandlerClassIsAnAbstractPointScanHandler() throws Exception {
        Class<?> parentClass = pointCodeCheckHandlerClass.getSuperclass();

        assertEquals("com.c15.rewardjar.core.point.AbstractPointScanHandler", parentClass.getName());
    }

    @Test
    public void testOverrideHandle() throws Exception {
        Class[] cArg = new Class[1];
        cArg[0] = PointAddRequest.class;

        Method handle = pointCodeCheckHandlerClass.getDeclaredMethod("handle", cArg);
        assertEquals("com.c15.rewardjar.core.point.PointAddRequest", handle.getReturnType().getTypeName());
        assertEquals(1, handle.getParameterCount());
        assertTrue(Modifier.isPublic(handle.getModifiers()));
    }

    @Test
    public void testHandleMethodQRCodeNotFound() throws Exception {
        lenient().when(merchantPointService.getMerchantPointsByQRCode("abcde")).thenReturn(null);
        PointAddRequest resultPointAddRequest = pointCodeCheckHandler.handle(pointAddRequest);
        assertNull(resultPointAddRequest);
    }

    @Test
    public void testHandleMethodQRCodeIsUsed() throws Exception {
        lenient().when(merchantPoints.isUsed()).thenReturn(true);
        lenient().when(merchantPointService.getMerchantPointsByQRCode("abcde")).thenReturn(merchantPoints);
        PointAddRequest resultPointAddRequest = pointCodeCheckHandler.handle(pointAddRequest);
        assertNull(resultPointAddRequest);
    }

    @Test
    public void testHandleMethodQRCodeSuccess() throws Exception {
        lenient().when(merchantPoints.isUsed()).thenReturn(false);
        lenient().when(merchantPointService.getMerchantPointsByQRCode("abcde")).thenReturn(mockMerchantPoints);
        PointAddRequest resultPointAddRequest = pointCodeCheckHandler.handle(pointAddRequest);
        assertNotNull(resultPointAddRequest);
    }

    @Test
    public void testHandleMethodAddPointToPointAddRequestWhenSuccess() throws Exception {
        lenient().when(merchantPoints.isUsed()).thenReturn(false);
        lenient().when(merchantPointService.getMerchantPointsByQRCode("abcde")).thenReturn(mockMerchantPoints);
        PointAddRequest resultPointAddRequest = pointCodeCheckHandler.handle(pointAddRequest);
        assertEquals(100, resultPointAddRequest.getPoint());
    }

    @Test
    public void testHandleMethodAddMerchantPointToPointAddRequestWhenSuccess() throws Exception {
        lenient().when(merchantPoints.isUsed()).thenReturn(false);
        lenient().when(merchantPointService.getMerchantPointsByQRCode("abcde")).thenReturn(mockMerchantPoints);
        PointAddRequest resultPointAddRequest = pointCodeCheckHandler.handle(pointAddRequest);
        MerchantPoints resultMerchantPoints = resultPointAddRequest.getMerchantPoints();
        assertNotNull(resultMerchantPoints);
        assertEquals(mockMerchantPoints, resultMerchantPoints);
    }

    @Test
    public void testHandleMethodVoidMerchantPointWhenSuccess() throws Exception {
        lenient().when(merchantPoints.isUsed()).thenReturn(false);
        lenient().when(merchantPointService.getMerchantPointsByQRCode("abcde")).thenReturn(mockMerchantPoints);
        PointAddRequest resultPointAddRequest = pointCodeCheckHandler.handle(pointAddRequest);
        MerchantPoints resultMerchantPoints = resultPointAddRequest.getMerchantPoints();
        verify(merchantPointService, times(1)).updateMerchantPoints(mockMerchantPoints, true);
    }
}
