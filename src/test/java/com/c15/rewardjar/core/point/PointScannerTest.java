package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.service.MerchantPointService;
import org.codehaus.groovy.runtime.MethodRankHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Assertions;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PointScannerTest {
    @InjectMocks
    private PointScanner pointScanner;

    @Mock
    private UserPointsAddHandler userPointsAddHandler;

    @Mock
    private UserPointsCheckHandler userPointsCheckHandler;

    @Mock
    private PointCodeCheckHandler pointCodeCheckHandler;

    private Class<?> pointScannerClass;
    private AppUser appUser;
    private PointAddRequest pointAddRequest;

    @BeforeEach
    public void setUp() throws Exception {
        appUser = new AppUser();
        pointScannerClass = Class.forName("com.c15.rewardjar.core.point.PointScanner");
        pointAddRequest = new PointAddRequest(appUser, "abcde");
    }

    @Test
    public void testPointScannerClassIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(pointScannerClass.getModifiers()));
    }

    @Test
    public void testMethodAddPointsExists() throws Exception {
        Class[] cArg = new Class[2];
        cArg[0] = AppUser.class;
        cArg[1] = String.class;

        Method addPointsMethod = pointScannerClass.getDeclaredMethod("addPoints", cArg);
        assertNotNull(addPointsMethod);
        assertTrue(Modifier.isPublic(addPointsMethod.getModifiers()));
        assertEquals(2, addPointsMethod.getParameterCount());
        assertEquals("com.c15.rewardjar.core.point.PointAddRequest", addPointsMethod.getReturnType().getTypeName());
    }

    @Test
    public void testMethodAddPointsChainOfResponsibilityPattern() throws Exception {
        pointScanner.addPoints(appUser, "abcde");
        verify(pointCodeCheckHandler, times(1)).setNext(userPointsCheckHandler);
        verify(userPointsCheckHandler, times(1)).setNext(userPointsAddHandler);
        verify(pointCodeCheckHandler, times(1)).handle(any());
    }

    @Test
    public void testMethodSetNextChainOfResponsibilityCallRealMethod() throws Exception {
        lenient().doCallRealMethod().when(pointCodeCheckHandler).setNext(any());
        lenient().doCallRealMethod().when(userPointsCheckHandler).setNext(any());
        lenient().doCallRealMethod().when(userPointsAddHandler).setNext(any());
        pointScanner.addPoints(appUser, "abcde");
    }

    @Test
    public void testMethodAddPointsAppUserAndCodeInsidePointAddRequest() throws Exception {
        lenient().when(pointCodeCheckHandler.handle(any())).thenReturn(pointAddRequest);
        PointAddRequest resultPointAddRequest = pointScanner.addPoints(appUser, "abcde");
        AppUser resultAppUser = resultPointAddRequest.getUser();
        String resultCode = resultPointAddRequest.getCode();
        assertEquals(appUser, resultAppUser);
        assertEquals("abcde", resultCode);
    }
}
