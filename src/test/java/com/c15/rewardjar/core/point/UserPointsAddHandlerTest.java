package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.UserPoints;
import com.c15.rewardjar.service.UserPointsService;
import org.codehaus.groovy.runtime.MethodRankHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Assertions;

import java.awt.*;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserPointsAddHandlerTest {
    @InjectMocks
    private UserPointsAddHandler userPointsAddHandler;

    @Mock
    private UserPointsService userPointsService;

    @Mock
    private UserPoints userPoints;

    private Class<?> userPointsAddHandlerClass;
    private PointAddRequest pointAddRequest;
    private AppUser appUser;

    @BeforeEach
    public void setUp() throws Exception {
        appUser = new AppUser();
        pointAddRequest = new PointAddRequest(appUser, "abcde");
        pointAddRequest.setPoint(100);
        pointAddRequest.setUserPoints(userPoints);
        userPointsAddHandlerClass = Class.forName("com.c15.rewardjar.core.point.UserPointsAddHandler");
    }

    @Test
    public void testUserPointsAddHandlerClassIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(userPointsAddHandlerClass.getModifiers()));
    }

    @Test
    public void testUserPointsAddHandlerClassIsAnAbstractPointScanHandler() throws Exception {
        Class<?> parentClass = userPointsAddHandlerClass.getSuperclass();

        assertEquals("com.c15.rewardjar.core.point.AbstractPointScanHandler", parentClass.getName());
    }

    @Test
    public void testOverrideHandle() throws Exception {
        Class[] cArg = new Class[1];
        cArg[0] = PointAddRequest.class;

        Method handle = userPointsAddHandlerClass.getDeclaredMethod("handle", cArg);
        assertEquals("com.c15.rewardjar.core.point.PointAddRequest", handle.getReturnType().getTypeName());
        assertEquals(1, handle.getParameterCount());
        assertTrue(Modifier.isPublic(handle.getModifiers()));
    }

    @Test
    public void testHandleMethod() throws Exception {
        pointAddRequest.setPoint(100);
        userPointsAddHandler.handle(pointAddRequest);
        verify(userPoints, times(1)).addPoint(100);
        verify(userPointsService, times(1)).updateUserPoints(userPoints);
    }

    @Test
    public void testHandleMethodReturn() throws Exception {
        pointAddRequest.setPoint(100);
        PointAddRequest resultPointAddRequest = userPointsAddHandler.handle(pointAddRequest);

        assertEquals(pointAddRequest, resultPointAddRequest);
    }
}
