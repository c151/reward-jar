package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.MerchantPoints;
import com.c15.rewardjar.model.UserPoints;
import com.c15.rewardjar.service.UserPointsService;
import org.codehaus.groovy.runtime.MethodRankHelper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.Assertions;

import java.awt.*;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserPointsCheckHandlerTest {
    @InjectMocks
    private UserPointsCheckHandler userPointsCheckHandler;

    @Mock
    private UserPointsService userPointsService;

    private Class<?> userPointsCheckHandlerClass;

    private AppUser user;
    private AppUser merchant;
    private MerchantPoints merchantPoints;
    private UserPoints userPoints;
    private PointAddRequest pointAddRequest;

    @BeforeEach
    public void setUp() throws Exception {
        userPointsCheckHandlerClass = Class.forName("com.c15.rewardjar.core.point.UserPointsCheckHandler");
        user = new AppUser();
        merchant = new AppUser();
        merchantPoints = new MerchantPoints();
        merchantPoints.setMerchant(merchant);
        userPoints = new UserPoints();
        userPoints.setUser(user);
        userPoints.setMerchant(merchant);
        pointAddRequest = new PointAddRequest(user, "abcde");
        pointAddRequest.setPoint(100);
        pointAddRequest.setMerchantPoints(merchantPoints);
    }

    @Test
    public void testUserPointsCheckHandlerClassIsConcreteClass() throws Exception {
        assertFalse(Modifier.isAbstract(userPointsCheckHandlerClass.getModifiers()));
    }

    @Test
    public void testUserPointsCheckHandlerClassIsAnAbstractPointScanHandler() throws Exception {
        Class<?> parentClass = userPointsCheckHandlerClass.getSuperclass();

        assertEquals("com.c15.rewardjar.core.point.AbstractPointScanHandler", parentClass.getName());
    }

    @Test
    public void testOverrideHandle() throws Exception {
        Class[] cArg = new Class[1];
        cArg[0] = PointAddRequest.class;

        Method handle = userPointsCheckHandlerClass.getDeclaredMethod("handle", cArg);
        assertEquals("com.c15.rewardjar.core.point.PointAddRequest", handle.getReturnType().getTypeName());
        assertEquals(1, handle.getParameterCount());
        assertTrue(Modifier.isPublic(handle.getModifiers()));
    }

    @Test
    public void testHandleMethodWhenUserPointsNotExists() throws Exception {
        lenient().when(userPointsService.findByUserAndMerchant(user, merchant)).thenReturn(null);
        PointAddRequest resultPointAddRequest = userPointsCheckHandler.handle(pointAddRequest);
        UserPoints resultUserPoints = resultPointAddRequest.getUserPoints();
        assertNotNull(resultUserPoints);
        assertEquals(user, resultUserPoints.getUser());
        assertEquals(merchant, resultUserPoints.getMerchant());
        verify(userPointsService, times(1)).createUserPoints(any());
    }

    @Test
    public void testHandleMethodWhenUserPointsExists() throws Exception {
        lenient().when(userPointsService.findByUserAndMerchant(user, merchant)).thenReturn(userPoints);
        PointAddRequest resultPointAddRequest = userPointsCheckHandler.handle(pointAddRequest);
        UserPoints resultUserPoints = resultPointAddRequest.getUserPoints();
        assertNotNull(resultUserPoints);
        assertEquals(user, resultUserPoints.getUser());
        assertEquals(merchant, resultUserPoints.getMerchant());
        verify(userPointsService, times(0)).createUserPoints(any());
    }
}
