package com.c15.rewardjar.core.point;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.UserPoints;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class UserPointsTest {
    private UserPoints userPoints;
    private AppUser user;
    private AppUser merchant;

    @BeforeEach
    public void setUp() throws Exception {
        userPoints = new UserPoints();
        user = new AppUser();
        merchant = new AppUser();
    }

    @Test
    public void testArgumentConsturctor() throws Exception {
        userPoints = new UserPoints(user, merchant);
        assertNotNull(userPoints);
    }

    @Test
    public void testGetIdMethod() throws Exception {
        assertNotNull(userPoints.getId());
    }

    @Test
    public void testGetterAndSetterUserMethod() throws Exception {
        userPoints.setUser(user);
        assertEquals(user, userPoints.getUser());
    }

    @Test
    public void testGetterAndSetterMerchantMethod() throws Exception {
        userPoints.setMerchant(merchant);
        assertEquals(merchant, userPoints.getMerchant());
    }

    @Test
    public void testGetterAndSetterPointMethod() {
        userPoints.setPoint(100);
        assertEquals(100, userPoints.getPoint());
    }

    @Test
    public void testAddPointMethod() {
        userPoints.setPoint(100);
        userPoints.addPoint(20);
        assertEquals(120, userPoints.getPoint());
    }
}
