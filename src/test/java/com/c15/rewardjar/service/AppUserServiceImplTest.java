package com.c15.rewardjar.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

import java.util.Optional;

import com.c15.rewardjar.core.user.EmailValidCheckHandler;
import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.AppUserRole;
import com.c15.rewardjar.repository.AppUserRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@ExtendWith(MockitoExtension.class)
public class AppUserServiceImplTest {

	@Autowired
	TestEntityManager entityManager;

	@Mock
	private AppUserRepository appUserRepository;

	@Mock
	private EmailValidCheckHandler emailValid;;

	@Spy
	private EmailValidator emailValidator;

	@Mock
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@InjectMocks
	private AppUserServiceImpl appUserService;

	private RegistrationRequestAppUser requestCreate;
	private AppUser appUserBefore;
	private AppUser appMerchantBefore;

	@BeforeEach
	public void setUp() {
		// String encodedPass = bCryptPasswordEncoder.encode("pass");
		appUserBefore = new AppUser();
		appUserBefore.setEmail("lala@lala.com");
		appUserBefore.setPassword("pass");
		appUserBefore.setFirstName("last");
		appUserBefore.setLastName("first");
		appUserBefore.setPhoneNumber("312789231");
		appUserBefore.setAppUserRole(AppUserRole.USER);
		appUserBefore.setIsVerified(true);
		appUserBefore.setPoint(0L);
		appUserBefore.setLocked(false);
		appUserBefore.setEnabled(true);

		appMerchantBefore = new AppUser();
		appMerchantBefore.setEmail("m@lala.com");
		appMerchantBefore.setPassword("pass");
		appMerchantBefore.setPhoneNumber("12789231");
		appMerchantBefore.setMerchantName("lmao");
		appMerchantBefore.setAppUserRole(AppUserRole.MERCHANT);
		appMerchantBefore.setIsVerified(true);
		appMerchantBefore.setPoint(0L);
		appMerchantBefore.setLocked(false);
		appMerchantBefore.setEnabled(true);
	}

	@Test
	public void testSignUpUser() {
		String email = "lala@lala.com";
		String password = "pass";
		String password2 = "pass";
		String firstName = "last";
		String lastName = "first";
		String merchantName = null;
		String phoneNumber = "312789231";

		RegistrationRequestAppUser requestCreate = new RegistrationRequestAppUser(email, password, password2, firstName,
				lastName, merchantName, phoneNumber) {
		};
		when(emailValidator.test("lala@lala.com")).thenReturn(true);
		when(bCryptPasswordEncoder.encode("pass")).thenReturn("pass");

		AppUser appUser = appUserService.signUpUser(requestCreate);
		assertEquals(appUserBefore, appUser);

	}

	@Test
	public void testSignUpMerchant() {
		String email = "m@lala.com";
		String password = "pass";
		String password2 = "pass";
		String firstName = "ast";
		String lastName = "irst";
		String merchantName = "lmao";
		String phoneNumber = "12789231";

		RegistrationRequestAppUser requestCreate = new RegistrationRequestAppUser(email, password, password2, firstName,
				lastName, merchantName, phoneNumber) {
		};
		when(emailValidator.test("m@lala.com")).thenReturn(true);
		when(bCryptPasswordEncoder.encode("pass")).thenReturn("pass");

		AppUser appUser = appUserService.signUpMerchant(requestCreate);
		assertEquals(appMerchantBefore, appUser);

	}

	@Test
	public void getUserByEmailTest() {
		Optional<AppUser> optAppUserBefore = Optional.ofNullable(appUserBefore);
		when(appUserRepository.findByEmail("lala@lala.com")).thenReturn(optAppUserBefore);
		AppUser appUser = appUserService.getUserByEmail("lala@lala.com");

		assertEquals(appUser, appUserBefore);
	}

	@Test
	public void getUserByMerchantName() {
		Optional<AppUser> optAppUserBefore = Optional.ofNullable(appMerchantBefore);
		when(appUserRepository.findByMerchantName("lmao")).thenReturn(optAppUserBefore);
		AppUser appUser = appUserService.getUserByMerchantName("lmao");

		assertEquals(appUser, appMerchantBefore);
	}

	@Test
	public void deleteByEmail() {
		Optional<AppUser> optAppUserBefore = Optional.ofNullable(appUserBefore);
		when(appUserRepository.findByEmail("lala@lala.com")).thenReturn(optAppUserBefore);
		appUserService.deleteByEmail("lala@lala.com");
		AppUser appUser = appUserService.getUserByEmail("lala@lala.com");

		assertEquals(appUser, appUserBefore);
	}

	@Test
	public void deleteByMerchantName() {
		Optional<AppUser> optAppUserBefore = Optional.ofNullable(appMerchantBefore);
		when(appUserRepository.findByMerchantName("lmao")).thenReturn(optAppUserBefore);
		appUserService.deleteByMerchantName("lmao");
		AppUser appUser = appUserService.getUserByMerchantName("lmao");

		assertEquals(appUser, appMerchantBefore);
	}

	@Test
	public void loadUserByUsernameTest() {
		Optional<AppUser> optAppUserBefore = Optional.ofNullable(appUserBefore);
		when(appUserRepository.findByEmail("lala@lala.com")).thenReturn(optAppUserBefore);
		UserDetails appUser = appUserService.loadUserByUsername("lala@lala.com");
		assertNotNull(appUser);
	}

	@Test
	public void signUpMerchantErrorTest() {
		String email = "m@lala.com";
		String password = "pass";
		String password2 = "pass";
		String firstName = "ast";
		String lastName = "irst";
		String merchantName = "lmao";
		String phoneNumber = "12789231";

		RegistrationRequestAppUser requestCreate = new RegistrationRequestAppUser(email, password, password2, firstName,
				lastName, merchantName, phoneNumber) {
		};
		lenient().when(emailValidator.test(email)).thenReturn(false);
		lenient().when(emailValid.handle(requestCreate)).thenReturn(false);
		lenient().when(appUserService.isValidAppUser(requestCreate)).thenReturn(false);
		// lenient().when(bCryptPasswordEncoder.encode(password)).thenReturn(password);
		// System.out.println(appUserService.isValidAppUser(requestCreate));
		Exception exception = assertThrows(IllegalStateException.class,
				() -> appUserService.signUpMerchant(requestCreate));

		assertTrue(exception.getMessage().contains("User is not valid!"));
	}

	@Test
	public void signUpUserErrorTest() {
		String email = "m@lala.com";
		String password = "pass";
		String password2 = "pass";
		String firstName = "ast";
		String lastName = "irst";
		String merchantName = "lmao";
		String phoneNumber = "12789231";

		RegistrationRequestAppUser requestCreate = new RegistrationRequestAppUser(email, password, password2, firstName,
				lastName, merchantName, phoneNumber) {
		};
		lenient().when(emailValidator.test(email)).thenReturn(false);
		lenient().when(emailValid.handle(requestCreate)).thenReturn(false);
		lenient().when(appUserService.isValidAppUser(requestCreate)).thenReturn(false);
		Exception exception = assertThrows(IllegalStateException.class, () -> appUserService.signUpUser(requestCreate));

		assertTrue(exception.getMessage().contains("User is not valid!"));
	}

	@Test
	public void loadUserErrorTest() {
		// String email = "m@lala.com";
		// lenient().when(appUserRepository.findByEmail(any(String.class))).thenReturn(null);
		// Exception exception = assertThrows(UsernameNotFoundException.class,
		// 		() -> appUserService.loadUserByUsername(email));

		// assertTrue(exception.getMessage().contains("User m@lala.com not found"));

	}

}
