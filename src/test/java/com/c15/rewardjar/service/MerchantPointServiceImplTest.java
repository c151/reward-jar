package com.c15.rewardjar.service;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.AppUserRole;
import com.c15.rewardjar.model.MerchantPoints;
import com.c15.rewardjar.repository.MerchantPointRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MerchantPointServiceImplTest {
    @Mock
    private MerchantPointRepository merchantPointRepository;

    @InjectMocks
    private MerchantPointServiceImpl merchantPointService;

    private AppUser merchant;
    private MerchantPoints first;
    private MerchantPoints second;
    private List<MerchantPoints> listOfPoints;

    @BeforeEach
    public void setUp() {
        merchant = new AppUser();
        merchant.setAppUserRole(AppUserRole.MERCHANT);

        first = new MerchantPoints(merchant, 2, "A", true);
        second = new MerchantPoints(merchant, 3, "B", true);

        listOfPoints = new ArrayList<>();
        listOfPoints.add(first);
        listOfPoints.add(second);
    }

    @Test
    public void whenCreateMerchantPointsIsCalledItShouldCreateValidMerchantPoint() {
        MerchantPoints merchantPoints = merchantPointService.createMerchantPoints(merchant, 100);
        assertEquals(merchant, merchantPoints.getMerchant());
        assertEquals(100, merchantPoints.getPoints());
        assertNotNull(merchantPoints.getQRCode());
        assertFalse(merchantPoints.isUsed());
    }

    @Test
    public void whenCreateMerchantPointsIsCalledItShouldCallMerchantPointRepositorySave() {
        merchantPointService.createMerchantPoints(merchant, 0);
        verify(merchantPointRepository, times(1)).save(any(MerchantPoints.class));
    }

    @Test
    public void whenGetMerchantPointsByQRCodeIsCalledItShouldReturnCorrectMerchantPoints() {
        MerchantPoints merchantPoints = new MerchantPoints(merchant, 0, "code", false);
        when(merchantPointRepository.findByQRCode("code")).thenReturn(merchantPoints);

        assertEquals(merchantPoints, merchantPointService.getMerchantPointsByQRCode("code"));
        verify(merchantPointRepository, times(1)).findByQRCode("code");
    }

    @Test
    public void whenGetTotalPointsSharedIsCalledItShouldReturnTotalSharedMerchantPoints() {
        when(merchantPointRepository.findByMerchantAndIsUsedTrue(merchant)).thenReturn(listOfPoints);

        assertEquals(5, merchantPointService.getTotalPointsShared(merchant));
        verify(merchantPointRepository, times(1)).findByMerchantAndIsUsedTrue(merchant);
    }

    @Test
    public void whenUpdateMerchantPointsIsCorrectlyImplemented() {
        second.setPoints(2);
        second.setQRCode("A");

        second.setUsed(false);
        when(merchantPointRepository.save(first)).thenReturn(second);
        MerchantPoints updatedFirst = merchantPointService.updateMerchantPoints(first, false);
        assertFalse(updatedFirst.isUsed());
        verify(merchantPointRepository, times(1)).save(second);

        first.setUsed(true);
        when(merchantPointRepository.save(second)).thenReturn(first);
        MerchantPoints updatedSecond = merchantPointService.updateMerchantPoints(second, true);
        assertTrue(updatedSecond.isUsed());
        verify(merchantPointRepository, times(2)).save(second);
    }
}
