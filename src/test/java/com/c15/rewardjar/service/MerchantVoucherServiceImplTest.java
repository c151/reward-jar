package com.c15.rewardjar.service;

import com.c15.rewardjar.model.AppUser;
import com.c15.rewardjar.model.AppUserRole;
import com.c15.rewardjar.model.MerchantVoucher;
import com.c15.rewardjar.model.UserVoucher;
import com.c15.rewardjar.repository.MerchantVoucherRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MerchantVoucherServiceImplTest {

    @Mock
    private MerchantVoucherRepository merchantVoucherRepository;

    @Mock
    private UserVoucherService userVoucherService;

    @InjectMocks
    private MerchantVoucherServiceImpl merchantVoucherServiceImpl;

    private AppUser merchant;
    private LocalDate dateToday;
    private LocalDate dateTomorrow;
    private MerchantVoucher merchantVoucher;
    private UserVoucher userVoucher;

    @BeforeEach
    public void setUp() {
        merchant = new AppUser();
        merchant.setAppUserRole(AppUserRole.MERCHANT);
        dateToday = LocalDate.now();
        dateTomorrow = dateToday.plusDays(1);
        merchantVoucher = new MerchantVoucher();
        merchantVoucher.setId(1);
        merchantVoucher.setVoucherName("test");
        merchantVoucher.setDescription("merchant voucher test");
        merchantVoucher.setImgURL("https://testImgURL.api");
        merchantVoucher.setStock(3);
        merchantVoucher.setPointRequired(4);
        merchantVoucher.setExpiredDate(dateToday);
        userVoucher = new UserVoucher();
    }

    @Test
    public void testCreateVoucher() {
        lenient().when(merchantVoucherServiceImpl.createVoucher(merchant,
                "test",
                "merchant voucher test",
                "https://testImgURL.api",
                3,
                4,
                dateToday)).thenReturn(merchantVoucher);

        MerchantVoucher expected = merchantVoucherServiceImpl.createVoucher(
                merchant,
                "test",
                "merchant voucher test",
                "https://testImgURL.api",
                3,
                4,
                dateToday);

        assertEquals(merchant, expected.getMerchant());
        assertEquals("test", expected.getVoucherName());
        assertEquals("merchant voucher test", expected.getDescription());
        assertEquals("https://testImgURL.api", expected.getImgURL());
        assertEquals(3, expected.getStock());
        assertEquals(4, expected.getPointRequired());
        assertEquals(dateToday, expected.getExpiredDate());

    }

    @Test
    public void testUpdateVoucher() {
        lenient().when(merchantVoucherServiceImpl.createVoucher(merchant,
                "test",
                "merchant voucher test",
                "https://testImgURL.api",
                3,
                4,
                dateToday)).thenReturn(merchantVoucher);

        MerchantVoucher expected = merchantVoucherServiceImpl.createVoucher(
                merchant,
                "test",
                "merchant voucher test",
                "https://testImgURL.api",
                3,
                4,
                dateToday);

        MerchantVoucher newVoucher = merchantVoucherServiceImpl.updateVoucher(
                1,
                merchant,
                "updated",
                "merchant voucher test updated",
                "https://updatedImgURL.api",
                4,
                3,
                dateTomorrow);

        assertEquals(merchant, newVoucher.getMerchant());
        assertEquals("updated", newVoucher.getVoucherName());
        assertEquals("merchant voucher test updated", newVoucher.getDescription());
        assertEquals("https://updatedImgURL.api", newVoucher.getImgURL());
        assertEquals(4, newVoucher.getStock());
        assertEquals(3, newVoucher.getPointRequired());
        assertEquals(dateTomorrow, newVoucher.getExpiredDate());
    }

    @Test
    public void testGetVoucherById() {
        lenient().when(merchantVoucherServiceImpl.getVoucherById(1)).thenReturn(merchantVoucher);
        MerchantVoucher expectedVoucher = merchantVoucherServiceImpl.getVoucherById(1);
        MerchantVoucher unexpectedVoucher = merchantVoucherServiceImpl.getVoucherById(0);
        Assertions.assertEquals(unexpectedVoucher, null);
        Assertions.assertEquals(expectedVoucher, merchantVoucher);
    }

    @Test
    public void testGetCountMerchantVoucherRedeemed() {
        ArrayList<MerchantVoucher> merchantVoucherArrayList = new ArrayList<>();
        merchantVoucherArrayList.add(merchantVoucher);
        ArrayList<UserVoucher> userVoucherArrayList = new ArrayList<>();
        userVoucherArrayList.add(userVoucher);
        lenient().when(merchantVoucherRepository.findByMerchant(merchant)).thenReturn(merchantVoucherArrayList);
        lenient().when(userVoucherService.getListUserVouchersByMerchantVouchers(merchantVoucher)).thenReturn(userVoucherArrayList);
        int count = merchantVoucherServiceImpl.getCountMerchantVoucherRedeemed(merchant);
        assertEquals(1, count);
    }

    @Test
    void testGetListMerchantVoucher() {
        Iterable<MerchantVoucher> listMerchantVoucher = merchantVoucherRepository.findByMerchant(merchant);
        lenient().when(merchantVoucherServiceImpl.getListMerchantVoucher(merchant)).thenReturn(listMerchantVoucher);
        Iterable<MerchantVoucher> listMerchantVoucherResult = merchantVoucherServiceImpl.getListMerchantVoucher(merchant);
        Assertions.assertIterableEquals(listMerchantVoucher, listMerchantVoucherResult);
    }
}
