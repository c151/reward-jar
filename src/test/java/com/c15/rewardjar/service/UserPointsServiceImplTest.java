package com.c15.rewardjar.service;
import com.c15.rewardjar.RewardJarApplication;
import com.c15.rewardjar.core.point.*;
import com.c15.rewardjar.model.*;
import com.c15.rewardjar.repository.UserPointsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class UserPointsServiceImplTest {
    @Mock
    private UserPointsRepository userPointsRepository;

    @InjectMocks
    private UserPointsServiceImpl userPointsService;

    @Mock
    private PointScanner pointScanner;

    private UserPoints userPoints;
    private AppUser appUser;
    private AppUser merchant;
    private PointAddRequest pointAddRequest;
    private MerchantPoints merchantPoints;
    private NewUserPoints newUserPoints;

    @BeforeEach
    public void setUp() {
        appUser = new AppUser();
        merchant = new AppUser();
        userPoints = new UserPoints();
        merchantPoints = new MerchantPoints();
        merchantPoints.setPoints(100);
        merchantPoints.setMerchant(merchant);
        pointAddRequest = new PointAddRequest(appUser, "abcde");
        pointAddRequest.setMerchantPoints(merchantPoints);
        pointAddRequest.setPoint(100);
        newUserPoints = new NewUserPoints(100, merchant);
    }

    @Test
    public void testServiceCreateUserPoints() {
        lenient().when(userPointsService.createUserPoints(userPoints)).thenReturn(userPoints);
        UserPoints newUserPoints = userPointsService.createUserPoints(userPoints);
        assertEquals(userPoints, newUserPoints);
    }

    @Test
    public void testServiceGetListUserPoints() {
        List<UserPoints> listUserPoints = userPointsRepository.findByUser(appUser);
        lenient().when(userPointsService.getListUserPoints(appUser)).thenReturn(listUserPoints);
        List<UserPoints> listUserPointsResult = userPointsService.getListUserPoints(appUser);
        Assertions.assertIterableEquals(listUserPoints, listUserPointsResult);
    }

    @Test
    public void testServiceUpdateUserPoints() {
        userPointsService.createUserPoints(userPoints);
        int currentPoint = userPoints.getPoint();
        userPoints.setPoint(1000);

        lenient().when(userPointsService.updateUserPoints(userPoints)).thenReturn(userPoints);
        UserPoints resultUserPoints = userPointsService.updateUserPoints(userPoints);

        assertNotEquals(resultUserPoints.getPoint(), currentPoint);
        assertEquals(resultUserPoints, userPoints);
    }

    @Test
    public void testServiceGetUserPointsByUserAndMerchant() {
        lenient().when(userPointsService.findByUserAndMerchant(appUser, merchant)).thenReturn(userPoints);
        UserPoints userPointsResult = userPointsService.findByUserAndMerchant(appUser, merchant);
        assertEquals(userPoints, userPointsResult);
    }

    @Test
    public void testServiceAddPointsByCode() {
        lenient().when(pointScanner.addPoints(any(), any())).thenReturn(pointAddRequest);
        NewUserPoints newUserPointsResult = userPointsService.addPointsByCode(appUser, "abcde");
        assertTrue(newUserPointsResult != null);
        assertEquals(100, newUserPointsResult.getPoints());
        assertEquals(merchant, newUserPointsResult.getMerchant());
    }

    @Test
    public void testServiceAddPointsByCodeWhenCodeNotFound() {
        lenient().when(pointScanner.addPoints(any(), any())).thenReturn(null);
        NewUserPoints newUserPointsResult = userPointsService.addPointsByCode(appUser, "abcde");
        assertNull(newUserPointsResult);
    }
}
