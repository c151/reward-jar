package com.c15.rewardjar.service;
import com.c15.rewardjar.model.*;
import com.c15.rewardjar.repository.MerchantVoucherRepository;
import com.c15.rewardjar.repository.UserPointsRepository;
import com.c15.rewardjar.repository.UserVoucherRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.lenient;


@ExtendWith(MockitoExtension.class)
public class UserVoucherServiceImplTest {
    @Mock
    private UserPointsRepository userPointsRepository;

    @Mock
    private UserVoucherRepository userVoucherRepository;

    @Mock
    private MerchantVoucherRepository merchantVoucherRepository;

    @InjectMocks
    private UserVoucherServiceImpl userVoucherService;

    private UserPoints userPoints;
    private UserVoucher userVoucher1;
    private UserVoucher userVoucher2;
    private MerchantVoucher merchantVoucher1;
    private MerchantVoucher merchantVoucher2;
    private AppUser appUser;
    private AppUser merchant;

    @BeforeEach
    public void setUp() {
        userPoints = new UserPoints();
        userVoucher1 = new UserVoucher();
        userVoucher2 = new UserVoucher();
        merchantVoucher1 = new MerchantVoucher();
        merchantVoucher2 = new MerchantVoucher();
        appUser = new AppUser();
        merchant = new AppUser();
        userVoucher1.setMerchantVoucher(merchantVoucher1);
        userVoucher1.getMerchantVoucher().setMerchant(merchant);
        userVoucher2.setMerchantVoucher(merchantVoucher2);
        userVoucher2.getMerchantVoucher().setMerchant(merchant);
    }

    @Test
    public void testCreateUserVoucher() {
        lenient().when(userVoucherService.createUserVoucher(userVoucher1)).thenReturn(userVoucher1);
        UserVoucher newUserVoucher = userVoucherService.createUserVoucher(userVoucher1);
        assertEquals(userVoucher1, newUserVoucher);
    }

    @Test
    public void testServiceGetListUserVouchers() {
        List<UserVoucher> userVoucherList = userVoucherRepository.findByUser(appUser);
        lenient().when(userVoucherService.getListUserVouchers(appUser)).thenReturn(userVoucherList);
        List<UserVoucher> userVouchersListResult = userVoucherService.getListUserVouchers(appUser);
        Assertions.assertIterableEquals(userVoucherList, userVouchersListResult);
    }

    @Test
    public void testServiceGetListUserVouchersByMerchantVouchers() {
        List<UserVoucher> userVoucherList = userVoucherRepository.findByMerchantVoucher(merchantVoucher1);
        lenient().when(userVoucherRepository.findByMerchantVoucher(merchantVoucher1)).thenReturn(userVoucherList);
        List<UserVoucher> userVouchersListResult = userVoucherService.getListUserVouchersByMerchantVouchers(merchantVoucher1);
        Assertions.assertIterableEquals(userVoucherList, userVouchersListResult);
    }

    @Test
    public void testServiceGetMapUserVouchers() {
        HashMap<AppUser, List<UserVoucher>> userVoucherMap = new HashMap<>();
        List<UserVoucher> userVoucherList = new ArrayList<>();

        merchant.setMerchantName("McDonalds");
        userVoucherList.add(userVoucher1);
        userVoucherList.add(userVoucher2);

        userVoucherMap.put(merchant, new ArrayList<>(Arrays.asList(userVoucher1, userVoucher2)));
        when(userVoucherRepository.findByUser(appUser)).thenReturn(userVoucherList);
        Assertions.assertEquals(userVoucherMap, userVoucherService.getMapUserVouchers(appUser));
    }

    @Test
    public void testServiceGetUserPointsByMerchantVoucher() {
        List<UserPoints> userPointsList = new ArrayList<>();
        userPoints.setMerchant(merchant);
        userPointsList.add(userPoints);
        when(userPointsRepository.findByUser(appUser)).thenReturn(userPointsList);

        merchantVoucher1.setMerchant(merchant);
        UserPoints userPointsResult = userVoucherService.getUserPointsByMerchantVoucher(appUser, merchantVoucher1);
        assertEquals(userPoints, userPointsResult);
    }

    @Test
    public void testServiceGetUserPointsByMerchantVoucherNull() {
        List<UserPoints> userPointsList = new ArrayList<>();
        userPoints.setMerchant(merchant);
        userPointsList.add(userPoints);
        when(userPointsRepository.findByUser(appUser)).thenReturn(userPointsList);

        AppUser merchant2 = new AppUser();
        merchant2.setMerchantName("Traveloka");
        merchantVoucher1.setMerchant(merchant2);
        UserPoints userPointsResult = userVoucherService.getUserPointsByMerchantVoucher(appUser, merchantVoucher1);
        assertNull(userPointsResult);
    }

    @Test
    public void testServiceGetListMerchantVouchers() {
        HashMap<UserPoints, List<MerchantVoucher>> merchantVouchers = new HashMap<>();
        List<UserPoints> userPointsList = new ArrayList<>();
        userPoints.setMerchant(merchant);
        userPointsList.add(userPoints);

        List<MerchantVoucher> merchantVoucherList = new ArrayList<>();
        merchantVoucherList.add(merchantVoucher1);
        merchantVouchers.put(userPoints, merchantVoucherList);

        when(merchantVoucherRepository.findByMerchant(userPoints.getMerchant())).thenReturn(merchantVoucherList);
        HashMap<UserPoints, List<MerchantVoucher>> userPointsResult = userVoucherService.getListMerchantVouchers(userPointsList);
        assertEquals(merchantVouchers, userPointsResult);
    }

    @Test
    public void testServiceRedeemVoucher() {
        merchantVoucher1.setStock(100);
        merchantVoucher1.setPointRequired(200);
        userPoints.setPoint(9000);
        userPoints.setMerchant(merchant);
        List<UserPoints> userPointsList = new ArrayList<>();
        userPointsList.add(userPoints);
        when(userPointsRepository.findByUser(appUser)).thenReturn(userPointsList);
        UserVoucher userVoucher = userVoucherService.redeemVoucher(appUser, merchantVoucher1);
        assertEquals(merchantVoucher1, userVoucher.getMerchantVoucher());
        assertEquals(99, merchantVoucher1.getStock());
        assertEquals(8800, userPoints.getPoint());
    }

    @Test
    public void testServiceRedeemVoucherFail() {
        merchantVoucher1.setStock(100);
        merchantVoucher1.setPointRequired(200);
        userPoints.setPoint(180);
        userPoints.setMerchant(merchant);
        List<UserPoints> userPointsList = new ArrayList<>();
        userPointsList.add(userPoints);
        when(userPointsRepository.findByUser(appUser)).thenReturn(userPointsList);
        UserVoucher userVoucher = userVoucherService.redeemVoucher(appUser, merchantVoucher1);
        assertEquals(null, userVoucher);
        assertEquals(100, merchantVoucher1.getStock());
        assertEquals(180, userPoints.getPoint());
    }

}
